package hr.fer.opp.gradska_groblja.dao;

import hr.fer.opp.gradska_groblja.model.Usluga;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UslugaRepository extends JpaRepository<Usluga, Long> {
}
