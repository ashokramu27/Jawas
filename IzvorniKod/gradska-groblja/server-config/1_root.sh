
# Add user jawas with group admin (root)
sudo adduser jawas
sudo usermod -aG admin jawas

# Add sudo permission
visudo

# Add swap to prevent npm from crashing on low memory
sudo fallocate -l 2G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab

# Copy content of sysctl.conf to given file:
sudo vim /etc/sysctl.conf

# Restart to enable swap
sudo shutdown -r now 

# Validate swap is configured correctly
sudo swapon --show

# Add ssh key pair
mkdir /home/jawas/.ssh/
cp ~/.ssh/authorized_keys /home/jawas/.ssh/authorized_keys
sudo chown jawas:jawas -R /home/jawas/.ssh/
sudo chmod 0700 -R /home/jawas/.ssh/
sudo chmod 0600 /home/jawas/.ssh/authorized_keys 

# Disable login with passwords (uncomment and set to no)
sudo vim /etc/ssh/ssh_config 
sudo service ssh restart 

