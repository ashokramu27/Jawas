package hr.fer.opp.gradska_groblja.service.implementation;

import hr.fer.opp.gradska_groblja.dao.SprovodRepository;
import hr.fer.opp.gradska_groblja.model.Pokojnik;
import hr.fer.opp.gradska_groblja.model.Sprovod;
import hr.fer.opp.gradska_groblja.service.ISprovodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class SprovodService implements ISprovodService {
    private final SprovodRepository sprovodRepository;

    @Autowired
    public SprovodService(SprovodRepository sprovodRepository) {
        this.sprovodRepository = sprovodRepository;
    }

    @Override
    public Sprovod add(LocalDateTime pocetak, Pokojnik pokojnik, String korisnikKreiranoIzmjena) {

        this.checkPrerequisites(pocetak, pokojnik);
        return sprovodRepository.save(new Sprovod(pocetak, pokojnik, LocalDateTime.now(), korisnikKreiranoIzmjena, LocalDateTime.now(),
                korisnikKreiranoIzmjena));
    }

    @Override
    public Sprovod save(Long id, LocalDateTime pocetak, Pokojnik pokojnik, String korisnikIzmjena) {

        this.checkPrerequisites(pocetak, pokojnik);
        LocalDateTime kreirano = sprovodRepository.getOne(id).getKreirano();
        String korisnikKreirano = sprovodRepository.getOne(id).getKorisnikKreirano();
        return sprovodRepository.save(new Sprovod(id, pocetak, pokojnik, kreirano, korisnikKreirano, LocalDateTime.now(), korisnikIzmjena));
    }

    @Override
    public Sprovod get(Long id) {
        return sprovodRepository.getOne(id);
    }

    @Override
    public List<Sprovod> findAll() {
        return sprovodRepository.findAll();
    }

    private void checkPrerequisites (LocalDateTime pocetak, Pokojnik pokojnik) {
        if (pocetak == null) {
            throw new IllegalArgumentException("Pocetak sprovoda can't be null or empty");
        }
        if (pokojnik == null) {
            throw new IllegalArgumentException("Pokojnik can't be null");
        }
    }
}
