package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.Obavijest;

import java.time.LocalTime;
import java.util.List;

public interface IObavijestService {
    Obavijest add (String naslov, String tekst, String slika, Groblje groblje, String korisnikKreiranoIzmjena) throws IllegalAccessException;

    Obavijest save (Long id, String naslov, String tekst, String slika, Groblje groblje, String korisnikIzmjena) throws IllegalAccessException;

    Obavijest get(Long id);

    List<Obavijest> findAll();
}
