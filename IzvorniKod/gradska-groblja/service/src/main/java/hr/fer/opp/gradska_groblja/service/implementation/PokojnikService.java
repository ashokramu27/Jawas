package hr.fer.opp.gradska_groblja.service.implementation;

import hr.fer.opp.gradska_groblja.dao.PokojnikRepository;
import hr.fer.opp.gradska_groblja.model.GrobnoMjesto;
import hr.fer.opp.gradska_groblja.model.Pokojnik;
import hr.fer.opp.gradska_groblja.service.IPokojnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class PokojnikService implements IPokojnikService {
    private final PokojnikRepository pokojnikRepository;

    @Autowired
    public PokojnikService(PokojnikRepository pokojnikRepository) {
        this.pokojnikRepository = pokojnikRepository;
    }

    @Override
    public Pokojnik add(String ime, String prezime, LocalDate datumRodenja, LocalDate datumSmrti, LocalDate datumUkopa, GrobnoMjesto grobnoMjesto,
                        String korisnikKreiranoIzmjena) {
        this.checkPrerequisites(ime, prezime, datumRodenja, datumSmrti, datumUkopa);
        return pokojnikRepository.save(new Pokojnik(ime, prezime, datumRodenja, datumSmrti, datumUkopa, grobnoMjesto, LocalDateTime.now(),
                korisnikKreiranoIzmjena, LocalDateTime.now(), korisnikKreiranoIzmjena));
    }

    @Override
    public Pokojnik save(Long id, String ime, String prezime, LocalDate datumRodenja, LocalDate datumSmrti, LocalDate datumUkopa,
                         GrobnoMjesto grobnoMjesto, String korisnikIzmjena) {

        this.checkPrerequisites(ime, prezime, datumRodenja, datumSmrti, datumUkopa);
        LocalDateTime kreirano = pokojnikRepository.getOne(id).getKreirano();
        String korisnikKreirano = pokojnikRepository.getOne(id).getKorisnikKreirano();
        return pokojnikRepository.save(new Pokojnik(id, ime, prezime, datumRodenja, datumSmrti, datumUkopa, grobnoMjesto, kreirano, korisnikKreirano,
                LocalDateTime.now(), korisnikIzmjena));
    }

    @Override
    public Pokojnik get(Long id) {
        return pokojnikRepository.getOne(id);
    }

    @Override
    public List<Pokojnik> findAll() {
        return pokojnikRepository.findAll();
    }

    private void checkPrerequisites(String ime, String prezime, LocalDate datumRodenja, LocalDate datumSmrti, LocalDate datumUkopa) {
        if (ime == null || ime.trim().isEmpty()) {
            throw new IllegalArgumentException("Ime can't be null or empty");
        }
        if (prezime == null || prezime.trim().isEmpty()) {
            throw new IllegalArgumentException("Prezime can't be null or empty");
        }
        if (datumRodenja != null && datumSmrti != null && datumUkopa != null) {
            if (datumRodenja.compareTo(datumSmrti) >= 0 || datumRodenja.compareTo(datumUkopa) >= 0 || datumSmrti.compareTo(datumUkopa) >= 0) {
                throw new IllegalArgumentException("Date chronology is not correct");
            }
        }
    }
}
