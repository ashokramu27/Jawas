package hr.fer.opp.gradska_groblja.service.implementation;

import hr.fer.opp.gradska_groblja.dao.LicnostRepository;
import hr.fer.opp.gradska_groblja.model.GrobnoMjesto;
import hr.fer.opp.gradska_groblja.model.Licnost;
import hr.fer.opp.gradska_groblja.service.ILicnostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class LicnostService implements ILicnostService {
    private final LicnostRepository licnostRepository;

    @Autowired
    public LicnostService(LicnostRepository licnostRepository) {
        this.licnostRepository = licnostRepository;
    }

    @Override
    public Licnost add(String ime, String prezime, LocalDate datumRodenja,
                       LocalDate datumSmrti, LocalDate datumUkopa, GrobnoMjesto grobnoMjesto, String slika, String opis, String korisnikKreiranoIzmjena) {

        this.checkPrerequisites(ime, prezime, datumRodenja, datumSmrti, datumUkopa);
        return licnostRepository.save(new Licnost(ime, prezime, datumRodenja, datumSmrti, datumUkopa, grobnoMjesto, slika, opis,
                LocalDateTime.now(), korisnikKreiranoIzmjena, LocalDateTime.now(), korisnikKreiranoIzmjena));
    }

    @Override
    public Licnost save(Long id, String ime, String prezime, LocalDate datumRodenja, LocalDate datumSmrti, LocalDate datumUkopa,
                        GrobnoMjesto grobnoMjesto, String slika, String opis, String korisnikIzmjena) {

        this.checkPrerequisites(ime, prezime, datumRodenja, datumSmrti, datumUkopa);
        LocalDateTime kreirano = licnostRepository.getOne(id).getKreirano();
        String korisnikKreirano = licnostRepository.getOne(id).getKorisnikKreirano();
        return licnostRepository.save(new Licnost(id, ime, prezime, datumRodenja, datumSmrti, datumUkopa, grobnoMjesto, slika, opis,
                kreirano, korisnikKreirano, LocalDateTime.now(), korisnikIzmjena));
    }

    @Override
    public Licnost get(Long id) {
        return licnostRepository.getOne(id);
    }

    @Override
    public List<Licnost> findAll() {
        return licnostRepository.findAll();
    }


    private void checkPrerequisites(String ime, String prezime, LocalDate datumRodenja, LocalDate datumSmrti, LocalDate datumUkopa) {
        if (ime == null || ime.trim().isEmpty()) {
            throw new IllegalArgumentException("Ime can't be null or empty");
        }
        if (prezime == null || prezime.trim().isEmpty()) {
            throw new IllegalArgumentException("Prezime can't be null or empty");
        }
        if (datumRodenja != null && datumSmrti != null && datumUkopa != null) {
            if (datumRodenja.compareTo(datumSmrti) >= 0 || datumRodenja.compareTo(datumUkopa) >= 0 || datumSmrti.compareTo(datumUkopa) >= 0) {
                throw new IllegalArgumentException("Date chronology is not correct");
            }
        }
    }
}
