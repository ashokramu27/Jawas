package hr.fer.opp.gradska_groblja.service.implementation;

import hr.fer.opp.gradska_groblja.dao.KucniLjubimacSpomenikRepository;
import hr.fer.opp.gradska_groblja.model.Korisnik;
import hr.fer.opp.gradska_groblja.model.KucniLjubimacSpomenik;
import hr.fer.opp.gradska_groblja.model.enums.Privatnost;
import hr.fer.opp.gradska_groblja.service.IKucniLjubimacSpomenikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class KucniLjubimacSpomenikService implements IKucniLjubimacSpomenikService {
    private final KucniLjubimacSpomenikRepository ljubimacRepository;

    @Autowired
    public KucniLjubimacSpomenikService(KucniLjubimacSpomenikRepository ljubimacRepository) {
        this.ljubimacRepository = ljubimacRepository;
    }

    @Override
    public KucniLjubimacSpomenik add(String ime, String vrsta, LocalDate datumRodenja, LocalDate datumSmrti, String opis,
                                     String slika, Privatnost privatnost, Korisnik korisnik, String korisnikKreiranoIzmjena) {
        if (ime == null || ime.trim().isEmpty()) {
            throw new IllegalArgumentException("Ime can't be null or empty");
        }
        if (vrsta == null || vrsta.trim().isEmpty()) {
            throw new IllegalArgumentException("Vrsta can't be null or empty");
        }
        if (privatnost == null) {
            privatnost = Privatnost.JAVNO;
        }
        return ljubimacRepository.save(new KucniLjubimacSpomenik(ime, vrsta, datumRodenja, datumSmrti, opis, slika, privatnost,
                korisnik, LocalDateTime.now(), korisnikKreiranoIzmjena, LocalDateTime.now(), korisnikKreiranoIzmjena));
    }

    @Override
    public KucniLjubimacSpomenik save(Long id, String ime, String vrsta, LocalDate datumRodenja, LocalDate datumSmrti,
                                      String opis, String slika, Privatnost privatnost, Korisnik korisnik, String korisnikIzmjena) {
        if (ime == null || ime.trim().isEmpty()) {
            throw new IllegalArgumentException("Ime can't be null or empty");
        }
        if (vrsta == null || vrsta.trim().isEmpty()) {
            throw new IllegalArgumentException("Vrsta can't be null or empty");
        }
        if (privatnost == null) {
            privatnost = Privatnost.JAVNO;
        }
        LocalDateTime kreirano = ljubimacRepository.getOne(id).getKreirano();
        String korisnikKreirano = ljubimacRepository.getOne(id).getKorisnikKreirano();
        return ljubimacRepository.save(new KucniLjubimacSpomenik(id, ime, vrsta, datumRodenja, datumSmrti, opis, slika,
                privatnost, korisnik, kreirano, korisnikKreirano, LocalDateTime.now(), korisnikIzmjena));
    }

    @Override
    public KucniLjubimacSpomenik get(Long id) {
        return ljubimacRepository.getOne(id);
    }

    @Override
    public List<KucniLjubimacSpomenik> findAll() {
        return ljubimacRepository.findAll();
    }

    @Override
    public List<KucniLjubimacSpomenik> findByKorisnik(Korisnik korisnik) {
        return ljubimacRepository.findByKorisnik(korisnik);
    }
}
