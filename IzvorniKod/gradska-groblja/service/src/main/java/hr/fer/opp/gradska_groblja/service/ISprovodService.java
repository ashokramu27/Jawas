package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.model.Pokojnik;
import hr.fer.opp.gradska_groblja.model.Sprovod;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;


public interface ISprovodService {

    Sprovod add(LocalDateTime pocetak, Pokojnik pokojnik, String korisnikKreiranoIzmjena) throws IllegalAccessException;

    Sprovod save(Long id, LocalDateTime pocetak, Pokojnik pokojnik, String korisnikKreiranoIzmjena) throws IllegalAccessException;

    Sprovod get(Long id);

    List<Sprovod> findAll();
}
