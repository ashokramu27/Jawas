package hr.fer.opp.gradska_groblja.service.implementation;

import hr.fer.opp.gradska_groblja.dao.ObavijestRepository;
import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.Obavijest;
import hr.fer.opp.gradska_groblja.service.IObavijestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ObavijestService implements IObavijestService{
    private final ObavijestRepository obavijestRepository;

    @Autowired
    public ObavijestService(ObavijestRepository obavijestRepository) {
        this.obavijestRepository = obavijestRepository;
    }

    @Override
    public Obavijest add(String naslov, String tekst, String slika, Groblje groblje, String korisnikKreiranoIzmjena){
        this.checkPrerequisites(naslov, tekst, slika, groblje);
        return obavijestRepository.save(new Obavijest(naslov, tekst, slika, groblje, LocalDateTime.now(), korisnikKreiranoIzmjena,
                LocalDateTime.now(), korisnikKreiranoIzmjena));
    }

    @Override
    public Obavijest save(Long id, String naslov, String tekst, String slika, Groblje groblje, String korisnikIzmjena){
        this.checkPrerequisites(naslov, tekst, slika, groblje);
        LocalDateTime kreirano = obavijestRepository.getOne(id).getKreirano();
        String korisnikKreirano = obavijestRepository.getOne(id).getKorisnikKreirano();
        return obavijestRepository.save(new Obavijest(naslov, tekst, slika, groblje, kreirano, korisnikKreirano, LocalDateTime.now(), korisnikIzmjena));
    }

    @Override
    public Obavijest get(Long id) {
        return obavijestRepository.getOne(id);
    }

    @Override
    public List<Obavijest> findAll() {
        return obavijestRepository.findAll();
    }

    private void checkPrerequisites(String naslov, String tekst, String slika, Groblje groblje) {
        if (naslov == null || naslov.trim().isEmpty()) {
            throw new IllegalArgumentException("Naslov can't be null or empty");
        }
        if (tekst == null || tekst.trim().isEmpty()) {
            throw new IllegalArgumentException("Tekst can't be null or empty");
        }
        if (groblje == null) {
            throw new IllegalArgumentException("Groblje can't be null or empty");
        }
    }
}
