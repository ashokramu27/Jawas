package hr.fer.opp.gradska_groblja.service.implementation;

import hr.fer.opp.gradska_groblja.dao.KorisnikUslugaGrobnoMjestoRepository;
import hr.fer.opp.gradska_groblja.model.GrobnoMjesto;
import hr.fer.opp.gradska_groblja.model.Korisnik;
import hr.fer.opp.gradska_groblja.model.KorisnikUslugaGrobnoMjesto;
import hr.fer.opp.gradska_groblja.model.Usluga;
import hr.fer.opp.gradska_groblja.service.IKorisnikUslugaGrobnoMjestoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class KorisnikUslugaGrobnoMjestoService implements IKorisnikUslugaGrobnoMjestoService {
    private final KorisnikUslugaGrobnoMjestoRepository repository;

    @Autowired
    public KorisnikUslugaGrobnoMjestoService(KorisnikUslugaGrobnoMjestoRepository repository) {
        this.repository = repository;
    }

    @Override
    public KorisnikUslugaGrobnoMjesto add(Korisnik korisnik, Usluga usluga, GrobnoMjesto grobnoMjesto, LocalDate datumIzvrsenja, String korisnikKreiranoIzmjena) {
        if(korisnik == null) {
            throw new IllegalArgumentException("Korisnik can't be null");
        }
        if(usluga == null) {
            throw new IllegalArgumentException("Usluga can't be null");
        }
        if(grobnoMjesto == null) {
            throw new IllegalArgumentException("Grobno mjesto can't be null");
        }
        if(datumIzvrsenja == null) {
            throw new IllegalArgumentException("Datum izvrsenja can't be null");
        }
        return repository.save(new KorisnikUslugaGrobnoMjesto(korisnik, usluga, grobnoMjesto, datumIzvrsenja, LocalDateTime.now(), korisnikKreiranoIzmjena,
                LocalDateTime.now(), korisnikKreiranoIzmjena));
    }

    @Override
    public KorisnikUslugaGrobnoMjesto save(Long id, Korisnik korisnik, Usluga usluga, GrobnoMjesto grobnoMjesto, LocalDate datumIzvrsenja, String korisnikIzmjena) {
        if(korisnik == null) {
            throw new IllegalArgumentException("Korisnik can't be null");
        }
        if(usluga == null) {
            throw new IllegalArgumentException("Usluga can't be null");
        }
        if(grobnoMjesto == null) {
            throw new IllegalArgumentException("Grobno mjesto can't be null");
        }
        if(datumIzvrsenja == null) {
            throw new IllegalArgumentException("Datum izvrsenja can't be null");
        }
        LocalDateTime kreirano = repository.getOne(id).getKreirano();
        String korisnikKreirano = repository.getOne(id).getKorisnikKreirano();
        return repository.save(new KorisnikUslugaGrobnoMjesto(id, korisnik, usluga, grobnoMjesto, datumIzvrsenja, kreirano, korisnikKreirano,
                LocalDateTime.now(), korisnikIzmjena));
    }

    @Override
    public KorisnikUslugaGrobnoMjesto get(Long id) {
        return repository.getOne(id);
    }

    @Override
    public List<KorisnikUslugaGrobnoMjesto> findAll() {
        return repository.findAll();
    }
}
