package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.model.GrobnoMjesto;
import hr.fer.opp.gradska_groblja.model.Pokojnik;

import java.time.LocalDate;
import java.util.List;

public interface IPokojnikService {

    Pokojnik add(String ime, String prezime, LocalDate datumRodenja, LocalDate datumSmrti, LocalDate datumUkopa, GrobnoMjesto grobnoMjesto,
                 String korisnikKreiranoIzmjena) throws IllegalAccessException;

    Pokojnik save(Long id, String ime, String prezime, LocalDate datumRodenja, LocalDate datumSmrti, LocalDate datumUkopa,
                  GrobnoMjesto grobnoMjesto, String korisnikKreiranoIzmjena) throws IllegalAccessException;

    Pokojnik get(Long id);

    List<Pokojnik> findAll();
}
