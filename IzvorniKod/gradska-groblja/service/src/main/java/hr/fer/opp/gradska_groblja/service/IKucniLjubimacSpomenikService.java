package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.model.Korisnik;
import hr.fer.opp.gradska_groblja.model.KucniLjubimacSpomenik;
import hr.fer.opp.gradska_groblja.model.enums.Privatnost;

import java.time.LocalDate;
import java.util.List;

public interface IKucniLjubimacSpomenikService {

    KucniLjubimacSpomenik add(String ime, String vrsta, LocalDate datumRodenja, LocalDate datumSmrti, String opis, String slika,
                              Privatnost privatnost, Korisnik korisnik, String korisnikKreiranoIzmjena) throws IllegalArgumentException;

    KucniLjubimacSpomenik save(Long id, String ime, String vrsta, LocalDate datumRodenja, LocalDate datumSmrti, String opis,
                               String slika, Privatnost privatnost, Korisnik korisnik, String korisnikIzmjena) throws IllegalArgumentException;

    KucniLjubimacSpomenik get(Long id);

    List<KucniLjubimacSpomenik> findAll();

    List<KucniLjubimacSpomenik> findByKorisnik(Korisnik korisnik);
}
