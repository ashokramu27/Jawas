package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.dao.GrobljeRepository;
import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.service.implementation.GrobljeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class GrobljeServiceTest {
    private GrobljeService grobljeService;

    private static final String NAZIV = "Mirogoj";
    private static final String ADRESA = "Aleja Hermanna Bollea 27";
    private static final String TELEFON = "01/4576342";
    private static final String URL = "mirogoj.hr";
    private static final String EMAIL = "mirogoj@groblja.hr";
    private static final LocalTime RADNIDANOD = LocalTime.of(0,0);
    private static final LocalTime RADNIDANDO = LocalTime.of(8,0);
    private static final LocalTime VIKENDOD = LocalTime.of(0,0);
    private static final LocalTime VIKENDDO = LocalTime.of(4,0);
    private static final LocalTime PRIJEMPOKOJNIKAOD = LocalTime.of(0,0);
    private static final LocalTime PRIJEMPOKOJNIKADO = LocalTime.of(6,0);
    private static final String SLIKA = "http://www.gradskagroblja.hr/UserDocsImages/galerije/mirogoj/GLAVNI-ULAZ-1.jpg";
    private static final String KARTA = "http://www.gradskagroblja.hr/UserDocsImages///mirogoj-stari.jpg";
    private static final LocalDateTime KREIRANO = LocalDateTime.now();
    private static final String KORISNIK_KREIRANO = "mseserko";
    private static final LocalDateTime IZMJENA = LocalDateTime.now();
    private static final String KORISNIK_IZMJENA = "mjukic";

    private static Groblje GROBLJE(String naziv, String adresa, String telefon, String url, String email, LocalTime radniDanOd, LocalTime radniDanDo, LocalTime vikendOd,
                                   LocalTime vikendDo, LocalTime prijemPokojnikaOd, LocalTime prijemPokojnikaDo, String slika, String karta){
        return new Groblje(naziv, adresa, telefon, url, email,radniDanOd, radniDanDo, vikendOd, vikendDo, prijemPokojnikaOd, prijemPokojnikaDo, slika,
                karta,KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    }

    @Mock
    private GrobljeRepository grobljeRepository;


    @Before
    @Autowired
    public void setUp(){
        this.grobljeService = new GrobljeService(grobljeRepository);
    }

    @Test
    public void testAddValidData() {
        Mockito.when(grobljeRepository.save(any(Groblje.class)))
                .thenReturn(GROBLJE(NAZIV, ADRESA, TELEFON, URL, EMAIL, RADNIDANOD, RADNIDANDO, VIKENDOD, VIKENDDO, PRIJEMPOKOJNIKAOD, PRIJEMPOKOJNIKADO, SLIKA, KARTA));
        Groblje result = grobljeService.add(NAZIV, ADRESA, TELEFON, URL, EMAIL, RADNIDANOD, RADNIDANDO, VIKENDOD, VIKENDDO, PRIJEMPOKOJNIKAOD, PRIJEMPOKOJNIKADO,
                SLIKA, KARTA, KORISNIK_KREIRANO);
        Groblje expected = new Groblje(NAZIV, ADRESA, TELEFON, URL, EMAIL, RADNIDANOD, RADNIDANDO, VIKENDOD, VIKENDDO, PRIJEMPOKOJNIKAOD, PRIJEMPOKOJNIKADO,
                SLIKA, KARTA, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
        grobljaEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyNaziv() {
        Mockito.when(grobljeRepository.save(any(Groblje.class)))
                .thenReturn(GROBLJE(" ", ADRESA, TELEFON, URL, EMAIL, RADNIDANOD, RADNIDANDO, VIKENDOD, VIKENDDO, PRIJEMPOKOJNIKAOD, PRIJEMPOKOJNIKADO,
                        SLIKA, KARTA));
        grobljeService.add(" ", ADRESA, TELEFON, URL, EMAIL, RADNIDANOD, RADNIDANDO, VIKENDOD, VIKENDDO, PRIJEMPOKOJNIKAOD, PRIJEMPOKOJNIKADO,
                SLIKA, KARTA, KORISNIK_KREIRANO);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testVikendOdGreaterThenVikendDo() {
        Mockito.when(grobljeRepository.save(any(Groblje.class)))
                .thenReturn(GROBLJE(NAZIV, ADRESA, TELEFON, URL, EMAIL, RADNIDANOD, RADNIDANDO, LocalTime.now(), VIKENDDO, PRIJEMPOKOJNIKAOD, PRIJEMPOKOJNIKADO,
                        SLIKA, KARTA));
        grobljeService.add(NAZIV, ADRESA, TELEFON, URL, EMAIL, RADNIDANOD, RADNIDANDO, LocalTime.now(), VIKENDDO, PRIJEMPOKOJNIKAOD, PRIJEMPOKOJNIKADO,
                SLIKA, KARTA, KORISNIK_KREIRANO);
    }

    private void grobljaEquals (Groblje expected, Groblje result) {
        assertEquals(expected.getNaziv(), result.getNaziv());
        assertEquals(expected.getAdresa(), result.getAdresa());
        assertEquals(expected.getTelefon(), result.getTelefon());
        assertEquals(expected.getUrl(), result.getUrl());
        assertEquals(expected.getRadniDanOd(), result.getRadniDanOd());
        assertEquals(expected.getRadniDanDo(), result.getRadniDanDo());
        assertEquals(expected.getVikendOd(), result.getVikendOd());
        assertEquals(expected.getVikendDo(), result.getVikendDo());
        assertEquals(expected.getPrijemPokojnikaOd(), result.getPrijemPokojnikaOd());
        assertEquals(expected.getPrijemPokojnikaDo(), result.getPrijemPokojnikaDo());
        assertEquals(expected.getSlika(), result.getSlika());
        assertEquals(expected.getKarta(), result.getKarta());
    }
}
