package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.dao.SprovodRepository;
import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.GrobnoMjesto;
import hr.fer.opp.gradska_groblja.model.Pokojnik;
import hr.fer.opp.gradska_groblja.model.Sprovod;
import hr.fer.opp.gradska_groblja.service.implementation.SprovodService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class SprovodServiceTest {
    private SprovodService sprovodService;

    private static final LocalDateTime KREIRANO = LocalDateTime.now();
    private static final String KORISNIK_KREIRANO = "mseserko";
    private static final LocalDateTime IZMJENA = LocalDateTime.now();
    private static final String KORISNIK_IZMJENA = "mjukic";
    private static final Groblje GROBLJE = new Groblje("Mirogoj", "Aleja Hermanna Bollea 27", "01/4576234", "mirogoj.hr",
            "mirogoj@groblja.hr", LocalTime.of(0, 0), LocalTime.of(8, 0), LocalTime.of(0, 0),
            LocalTime.of(4, 0), LocalTime.of(0, 0), LocalTime.of(6, 0),
            "http://www.gradskagroblja.hr/UserDocsImages/galerije/mirogoj/GLAVNI-ULAZ-1.jpg",
            "http://www.gradskagroblja.hr/UserDocsImages///mirogoj-stari.jpg", KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    private static final GrobnoMjesto GROBNO_MJESTO = new GrobnoMjesto("1A", (short) 1, (short) 1, 1, GROBLJE,
            KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    private static final LocalDateTime POCETAK = LocalDateTime.now();
    private static final Pokojnik POKOJNIK = new Pokojnik("Josip", "Broz", LocalDate.of(1892, 5, 7),
            LocalDate.of(1980, 5, 4), LocalDate.now(), GROBNO_MJESTO,  KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);

    private static Sprovod SPROVOD(LocalDateTime pocetak, Pokojnik pokojnik){
        return new Sprovod(pocetak, pokojnik, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    }

    @Mock
    private SprovodRepository sprovodRepository;

    @Before
    @Autowired
    public void setUp(){
        this.sprovodService = new SprovodService(sprovodRepository);
    }

    @Test
    public void testAddValidData() {
        Mockito.when(sprovodRepository.save(any(Sprovod.class)))
                .thenReturn(SPROVOD(POCETAK, POKOJNIK));
        Sprovod result = sprovodService.add(POCETAK, POKOJNIK, KORISNIK_KREIRANO);
        Sprovod expected = new Sprovod(POCETAK, POKOJNIK, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
        sprovodEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyPocetak() {
        Mockito.when(sprovodRepository.save(any(Sprovod.class)))
                .thenReturn(SPROVOD(null, POKOJNIK));
        sprovodService.add(null, POKOJNIK, KORISNIK_KREIRANO);
    }

    private void sprovodEquals (Sprovod expected, Sprovod result) {
        assertEquals(expected.getPocetak(), result.getPocetak());
        assertEquals(expected.getPokojnik(), result.getPokojnik());
    }
}
