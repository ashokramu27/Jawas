package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.dao.KucniLjubimacSpomenikRepository;
import hr.fer.opp.gradska_groblja.model.Korisnik;
import hr.fer.opp.gradska_groblja.model.KucniLjubimacSpomenik;
import hr.fer.opp.gradska_groblja.model.enums.Privatnost;
import hr.fer.opp.gradska_groblja.service.implementation.KucniLjubimacSpomenikService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class KucniLjubimciSpomenikServiceTest {
    private KucniLjubimacSpomenikService ljubimciService;

    private static final String IME = "Bigi";
    private static final String VRSTA = "njemacki ovcar";
    private static final LocalDate DATUM_RODENJA = LocalDate.of(2006, 11, 6);
    private static final LocalDate DATUM_SMRTI = LocalDate.of(2019, 5, 12);
    private static final String OPIS = "";
    private static final String SLIKA = "http://www.insidedogsworld.com/wp-content/uploads/2017/07/gs30-k.jpg";
    private static final Privatnost PRIVATNOST = Privatnost.JAVNO;
    private static final Korisnik KORISNIK = new Korisnik();
    private static final LocalDateTime KREIRANO = LocalDateTime.now();
    private static final String KORISNIK_KREIRANO = "skuzmic";
    private static final LocalDateTime IZMJENA = LocalDateTime.now();
    private static final String KORISNIK_IZMJENA = "skuzmic";

    private static KucniLjubimacSpomenik LJUBIMAC(String ime, String vrsta, Privatnost privatnost, Korisnik korisnik) {
        return new KucniLjubimacSpomenik(ime, vrsta, DATUM_RODENJA, DATUM_SMRTI, OPIS, SLIKA, privatnost, korisnik,
                KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    }

    @Mock
    private KucniLjubimacSpomenikRepository ljubimacRepository;

    @Before
    @Autowired
    public void setUp() {
        this.ljubimciService = new KucniLjubimacSpomenikService(ljubimacRepository);
    }

    @Test
    public void testAddValidData() {
        Mockito.when(ljubimacRepository.save(any(KucniLjubimacSpomenik.class)))
                .thenReturn(LJUBIMAC(IME, VRSTA, PRIVATNOST, KORISNIK));
        KucniLjubimacSpomenik result = ljubimciService.add(IME, VRSTA, DATUM_RODENJA, DATUM_SMRTI, OPIS, SLIKA, PRIVATNOST, KORISNIK,
                KORISNIK_KREIRANO);
        KucniLjubimacSpomenik expected = new KucniLjubimacSpomenik(IME, VRSTA, DATUM_RODENJA, DATUM_SMRTI, OPIS, SLIKA, PRIVATNOST, KORISNIK,
                KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
        ljubimacEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyIme() {
        Mockito.when(ljubimacRepository.save(any(KucniLjubimacSpomenik.class)))
                .thenReturn(LJUBIMAC(" ", VRSTA, PRIVATNOST, KORISNIK));
        ljubimciService.add(" ", VRSTA, DATUM_RODENJA, DATUM_SMRTI, OPIS, SLIKA, PRIVATNOST, KORISNIK, KORISNIK_KREIRANO);
    }

    private void ljubimacEquals (KucniLjubimacSpomenik expected, KucniLjubimacSpomenik result) {
        assertEquals(expected.getIme(), result.getIme());
        assertEquals(expected.getVrsta(), result.getVrsta());
        assertEquals(expected.getDatumRodenja(), result.getDatumRodenja());
        assertEquals(expected.getDatumSmrti(), result.getDatumSmrti());
        assertEquals(expected.getOpis(), result.getOpis());
        assertEquals(expected.getSlika(), result.getSlika());
        assertEquals(expected.getPrivatnost(), result.getPrivatnost());
    }
}
