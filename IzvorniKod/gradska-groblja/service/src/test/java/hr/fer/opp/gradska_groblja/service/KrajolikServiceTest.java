package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.dao.KrajolikRepository;
import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.Krajolik;
import hr.fer.opp.gradska_groblja.service.implementation.KrajolikService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class KrajolikServiceTest {
    private KrajolikService krajolikService;

    private static final LocalDateTime KREIRANO = LocalDateTime.now();
    private static final String KORISNIK_KREIRANO = "mseserko";
    private static final LocalDateTime IZMJENA = LocalDateTime.now();
    private static final String KORISNIK_IZMJENA = "mjukic";
    private static final String NAZIV = "Arkade";
    private static final String OPIS = "Počivalište poznatih i manje poznatih";
    private static final String SLIKA = "http://www.gradskagroblja.hr/UserDocsImages/galerije/mirogoj/ARKADE---UNUTARNJA-STRANA-5.jpg";
    private static final String LOKACIJA = "68.1.19, 45.34.2";
    private static final Groblje GROBLJE = new Groblje("Mirogoj", "Aleja Hermanna Bollea 27", "01/4576234", "mirogoj.hr",
            "mirogoj@groblja.hr", LocalTime.of(0, 0), LocalTime.of(8, 0), LocalTime.of(0, 0),
            LocalTime.of(4, 0), LocalTime.of(0, 0), LocalTime.of(6, 0),
            "http://www.gradskagroblja.hr/UserDocsImages/galerije/mirogoj/GLAVNI-ULAZ-1.jpg",
            "http://www.gradskagroblja.hr/UserDocsImages///mirogoj-stari.jpg", KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);

    private static Krajolik KRAJOLIK(String naziv, String opis, String slika, String lokacija, Groblje groblje){
        return new Krajolik(naziv, opis, slika, lokacija, groblje, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    }

    @Mock
    private KrajolikRepository krajolikRepository;

    @Before
    @Autowired
    public void setUp(){
        this.krajolikService = new KrajolikService(krajolikRepository);
    }

    @Test
    public void testAddValidData() {
        Mockito.when(krajolikRepository.save(any(Krajolik.class)))
                .thenReturn(KRAJOLIK(NAZIV, OPIS, SLIKA, LOKACIJA, GROBLJE));
        Krajolik result = krajolikService.add(NAZIV, OPIS, SLIKA, LOKACIJA, GROBLJE, KORISNIK_KREIRANO);
        Krajolik expected = new Krajolik(NAZIV, OPIS, SLIKA, LOKACIJA, GROBLJE, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
        krajolikEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyNaziv() {
        Mockito.when(krajolikRepository.save(any(Krajolik.class)))
                .thenReturn(KRAJOLIK("", OPIS, SLIKA, LOKACIJA, GROBLJE));
        krajolikService.add("", OPIS, SLIKA, LOKACIJA, GROBLJE, KORISNIK_KREIRANO);
    }

    private void krajolikEquals (Krajolik expected, Krajolik result) {
        assertEquals(expected.getNaziv(), result.getNaziv());
        assertEquals(expected.getOpis(), result.getOpis());
        assertEquals(expected.getSlika(), result.getSlika());
        assertEquals(expected.getLokacija(), result.getLokacija());
        assertEquals(expected.getGroblje(), result.getGroblje());
    }
}
