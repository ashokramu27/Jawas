package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.dao.GrobnoMjestoRepository;
import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.GrobnoMjesto;
import hr.fer.opp.gradska_groblja.service.implementation.GrobnoMjestoService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class GrobnoMjestoServiceTest {
    private GrobnoMjestoService grobnoMjestoService;

    private static final LocalDateTime KREIRANO = LocalDateTime.now();
    private static final String KORISNIK_KREIRANO = "mseserko";
    private static final LocalDateTime IZMJENA = LocalDateTime.now();
    private static final String KORISNIK_IZMJENA = "mjukic";
    private static final String ODJEL = "1A";
    private static final Short POLJE = 1;
    private static final Short RAZRED = 1;
    private static final Integer BROJ = 1;
    private static final Groblje GROBLJE = new Groblje("Mirogoj", "Aleja Hermanna Bollea 27", "01/4576234", "mirogoj.hr",
            "mirogoj@groblja.hr", LocalTime.of(0,0), LocalTime.of(8,0), LocalTime.of(0,0),
            LocalTime.of(4,0), LocalTime.of(0,0), LocalTime.of(6,0),
            "http://www.gradskagroblja.hr/UserDocsImages/galerije/mirogoj/GLAVNI-ULAZ-1.jpg",
            "http://www.gradskagroblja.hr/UserDocsImages///mirogoj-stari.jpg", KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);

    private static GrobnoMjesto GROBNO_MJESTO(String odjel, Short polje, Short razred, Integer broj, Groblje groblje){
        return new GrobnoMjesto(odjel, polje, razred, broj, groblje, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    }

    @Mock
    private GrobnoMjestoRepository grobnoMjestoRepository;

    @Before
    @Autowired
    public void setUp(){
        this.grobnoMjestoService = new GrobnoMjestoService(grobnoMjestoRepository);
    }

    @Test
    public void testAddValidData() {
        Mockito.when(grobnoMjestoRepository.save(any(GrobnoMjesto.class)))
                .thenReturn(GROBNO_MJESTO(ODJEL, POLJE, RAZRED, BROJ, GROBLJE));
        GrobnoMjesto result = grobnoMjestoService.add(ODJEL, POLJE, RAZRED, BROJ, GROBLJE, KORISNIK_KREIRANO);
        GrobnoMjesto expected = new GrobnoMjesto(ODJEL, POLJE, RAZRED, BROJ, GROBLJE, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
        grobnaMjestaEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyBroj() {
        Mockito.when(grobnoMjestoRepository.save(any(GrobnoMjesto.class)))
                .thenReturn(GROBNO_MJESTO(ODJEL, POLJE, RAZRED, null, GROBLJE));
        grobnoMjestoService.add(ODJEL, POLJE, RAZRED, null, GROBLJE, KORISNIK_KREIRANO);
    }

    private void grobnaMjestaEquals (GrobnoMjesto expected, GrobnoMjesto result) {
        assertEquals(expected.getOdjel(), result.getOdjel());
        assertEquals(expected.getPolje(), result.getPolje());
        assertEquals(expected.getRazred(), result.getRazred());
        assertEquals(expected.getGroblje(), result.getGroblje());
    }
}
