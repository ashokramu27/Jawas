package hr.fer.opp.gradska_groblja.web.controller;

import hr.fer.opp.gradska_groblja.model.Korisnik;
import hr.fer.opp.gradska_groblja.model.KucniLjubimacSpomenik;
import hr.fer.opp.gradska_groblja.model.enums.Uloga;
import hr.fer.opp.gradska_groblja.service.IKorisnikService;
import hr.fer.opp.gradska_groblja.service.IKucniLjubimacSpomenikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/korisnik")
public class KorisnikController {
    private final IKorisnikService korisnikService;
    private final IKucniLjubimacSpomenikService ljubimacService;
    private final int passwordMinLength;
    private final int oibLength;

    @Autowired
    public KorisnikController(IKorisnikService korisnikService, IKucniLjubimacSpomenikService ljubimacService,
                              @Value("${limits.password.min_length}") int passwordMinLength,
                              @Value("${limits.oib.length}") int oibLength) {
        this.korisnikService = korisnikService;
        this.ljubimacService = ljubimacService;
        this.passwordMinLength = passwordMinLength;
        this.oibLength = oibLength;
    }

    @PostMapping
    public ResponseEntity<?> add(String oib, String ime, String prezime, String korisnickoIme, String lozinka, Uloga uloga) {
        if (oib == null || oib.trim().length() != oibLength)
            return ResponseEntity.badRequest().body("OIB is invalid");

        if (ime == null || ime.trim().isEmpty())
            return ResponseEntity.badRequest().body("User name can not be null or empty");

        if (prezime == null || prezime.trim().isEmpty())
            return ResponseEntity.badRequest().body("User surnamename can not be null or empty");

        if (korisnickoIme == null || korisnickoIme.trim().isEmpty())
            return ResponseEntity.badRequest().body("Username can not be null or empty");

        if (lozinka == null || lozinka.trim().isEmpty())
            return ResponseEntity.badRequest().body("Password can not be null or empty");

        if (lozinka.trim().length() < passwordMinLength)
            return ResponseEntity.badRequest().body("Password must be at least " + passwordMinLength + " characters long");

        if (uloga == null)
            uloga = Uloga.KORISNIK;

        try {
            Korisnik korisnik = korisnikService.add(oib.trim(), ime.trim(), prezime.trim(), korisnickoIme.trim(), lozinka.trim(), uloga, "lkristof");
            return ResponseEntity.ok(korisnik);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Korisnik body) {
        if (body.getOib() == null || body.getOib().trim().length() != oibLength)
            return ResponseEntity.badRequest().body("OIB is invalid");

        if (body.getIme() == null || body.getIme().trim().isEmpty())
            return ResponseEntity.badRequest().body("User name can not be null or empty");

        if (body.getPrezime() == null || body.getPrezime().trim().isEmpty())
            return ResponseEntity.badRequest().body("User surnamename can not be null or empty");

        if (body.getKorisnickoIme() == null || body.getKorisnickoIme().trim().isEmpty())
            return ResponseEntity.badRequest().body("Username can not be null or empty");

        if (body.getLozinka() == null || body.getLozinka().trim().isEmpty())
            return ResponseEntity.badRequest().body("Password can not be null or empty");

        if (body.getLozinka().trim().length() < passwordMinLength)
            return ResponseEntity.badRequest().body("Password must be at least " + passwordMinLength + " characters long");

        if (body.getUloga() == null)
            body.setUloga(Uloga.KORISNIK);

        try {
            Korisnik korisnik = korisnikService.save(id, body.getOib(), body.getIme(), body.getPrezime(), body.getKorisnickoIme(), body.getLozinka(),
                    body.getUloga(), "lkristof");
            return ResponseEntity.ok(korisnik);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Korisnik get(@PathVariable Long id) {
        return korisnikService.get(id);
    }

    @GetMapping
    public List<Korisnik> findAll() {
        return korisnikService.findAll();
    }

    @GetMapping("/ljubimci/{id}")
    public List<KucniLjubimacSpomenik> getKorisnikLjubimce(@PathVariable Long id) {
        return ljubimacService.findByKorisnik(korisnikService.get(id));
    }
}
