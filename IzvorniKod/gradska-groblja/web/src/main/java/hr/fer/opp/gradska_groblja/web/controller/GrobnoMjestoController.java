package hr.fer.opp.gradska_groblja.web.controller;

import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.GrobnoMjesto;
import hr.fer.opp.gradska_groblja.service.IGrobljeService;
import hr.fer.opp.gradska_groblja.service.IGrobnoMjestoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/grobnoMjesto")
public class GrobnoMjestoController {
    private final IGrobnoMjestoService grobnoMjestoService;
    private final IGrobljeService grobljeService;

    @Autowired
    public GrobnoMjestoController(IGrobnoMjestoService grobnoMjestoService, IGrobljeService grobljeService) {
        this.grobnoMjestoService = grobnoMjestoService;
        this.grobljeService = grobljeService;
    }

    //u postmanu testirate da u body odaberete form-data
    @PostMapping
    public ResponseEntity<?> add(String odjel, Short polje, Short razred, Integer broj, Long grobljeId) {
        if (odjel == null || odjel.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Odjel can't be null or empty");
        }
        if (polje == null) {
            return ResponseEntity.badRequest().body("Polje can't be null or empty");
        }
        if (razred == null) {
            return ResponseEntity.badRequest().body("Razred can't be null or empty");
        }
        if (broj == null) {
            return ResponseEntity.badRequest().body("Broj can't be null or empty");
        }
        if (polje < 0 || razred < 0 || broj < 0) {
            return ResponseEntity.badRequest().body("Polje, razred and broj can`t be negative values");
        }
        if (grobljeId == null) {
            return ResponseEntity.badRequest().body("GrobljeId can't be null or empty");
        }
        Groblje groblje =  grobljeService.get(grobljeId);
        if (groblje == null) {
            return ResponseEntity.badRequest().body("Can't find groblje with given id");
        }

        try {
            GrobnoMjesto grobnoMjesto = grobnoMjestoService.add(odjel.trim(), polje, razred, broj, groblje, "lkristof");
            return ResponseEntity.ok(grobnoMjesto);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    //u postmanu testirate da u body stavite raw - json
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody GrobnoMjesto body) {
        if (body.getOdjel() == null || body.getOdjel().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Odjel can't be null or empty");
        }
        if (body.getPolje() == null) {
            return ResponseEntity.badRequest().body("Polje can't be null or empty");
        }
        if (body.getRazred() == null) {
            return ResponseEntity.badRequest().body("Razred can't be null or empty");
        }
        if (body.getBroj() == null) {
            return ResponseEntity.badRequest().body("Broj can't be null or empty");
        }
        if (body.getBroj() < 0 || body.getPolje() < 0 || body.getRazred() < 0) {
            return ResponseEntity.badRequest().body("Polje, razred and broj can`t be negative values");
        }
        try {
            GrobnoMjesto grobnoMjesto = grobnoMjestoService.save(id, body.getOdjel().trim(), body.getPolje(), body.getRazred(), body.getBroj(), body.getGroblje(),
                    "lkristof");
            return ResponseEntity.ok(grobnoMjesto);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public GrobnoMjesto get(@PathVariable Long id) {
        return grobnoMjestoService.get(id);
    }

    @GetMapping
    public List<GrobnoMjesto> findAll() {
        return grobnoMjestoService.findAll();
    }

}
