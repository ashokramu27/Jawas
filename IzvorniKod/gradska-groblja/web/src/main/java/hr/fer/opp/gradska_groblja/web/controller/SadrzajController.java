package hr.fer.opp.gradska_groblja.web.controller;

import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.Sadrzaj;
import hr.fer.opp.gradska_groblja.model.enums.VrstaSadrzaja;
import hr.fer.opp.gradska_groblja.service.IGrobljeService;
import hr.fer.opp.gradska_groblja.service.ISadrzajService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/sadrzaj")
public class SadrzajController {
    private final ISadrzajService sadrzajService;
    private final IGrobljeService grobljeService;

    @Autowired
    public SadrzajController(ISadrzajService sadrzajService, IGrobljeService grobljeService) {
        this.sadrzajService = sadrzajService;
        this.grobljeService = grobljeService;
    }

    //u postmanu testirate da u body odaberete form-data
    @PostMapping
    public ResponseEntity<?> add(VrstaSadrzaja vrsta, String lokacija, Long grobljeId) {
        if (vrsta == null) {
            return ResponseEntity.badRequest().body("Vrsta can't be null or empty");
        }
        if (lokacija == null || lokacija.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Lokacija can't be null or empty");
        }

        if (grobljeId == null) {
            return ResponseEntity.badRequest().body("GrobljeId can't be null or empty");
        }
        Groblje groblje = grobljeService.get(grobljeId);
        if (groblje == null) {
            return ResponseEntity.badRequest().body("Can't find groblje with given id");
        }

        try {
            Sadrzaj sadrzaj = sadrzajService.add(vrsta, lokacija.trim(), groblje, "lkristof");
            return ResponseEntity.ok(sadrzaj);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    //u postmanu testirate da u body stavite raw - json
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Sadrzaj body) {
        if (body.getVrsta() == null) {
            return ResponseEntity.badRequest().body("Vrsta can't be null or empty");
        }
        if (body.getLokacija() == null || body.getLokacija().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Lokacija can't be null or empty");
        }
        try {
            Sadrzaj sadrzaj = sadrzajService.save(id, body.getVrsta(), body.getLokacija().trim(), body.getGroblje(), "lkristof");
            return ResponseEntity.ok(sadrzaj);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Sadrzaj get(@PathVariable Long id) {
        return sadrzajService.get(id);
    }

    @GetMapping
    public List<Sadrzaj> findAll() {
        return sadrzajService.findAll();
    }

}
