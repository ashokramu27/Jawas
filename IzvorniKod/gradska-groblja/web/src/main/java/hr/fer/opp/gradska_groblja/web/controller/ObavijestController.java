package hr.fer.opp.gradska_groblja.web.controller;

import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.Obavijest;
import hr.fer.opp.gradska_groblja.service.IGrobljeService;
import hr.fer.opp.gradska_groblja.service.IObavijestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/obavijest")
public class ObavijestController {
    private final IObavijestService obavijestService;
    private final IGrobljeService grobljeService;

    @Autowired
    public ObavijestController(IObavijestService obavijestService, IGrobljeService grobljeService){
        this.obavijestService = obavijestService;
        this.grobljeService = grobljeService;
    }

    @PostMapping
    public ResponseEntity<?> add(String naslov, String tekst, String slika, Long grobljeId){
        if (naslov == null || naslov.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Naslov can't be null or empty");
        }
        if (tekst == null || tekst.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Tekst can't be null or empty");
        }
        if (grobljeId == null) {
            return ResponseEntity.badRequest().body("GrobljeId can't be null or empty");
        }
        Groblje groblje = grobljeService.get(grobljeId);
        if (groblje == null) {
            return ResponseEntity.badRequest().body("Can't find groblje with given id");
        }
        try {
            Obavijest obavijest = obavijestService.add(naslov.trim(), tekst.trim(), slika.trim(), groblje, "lkristof");
            return ResponseEntity.ok(obavijest);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Obavijest body){
        if (body.getNaslov() == null || body.getNaslov().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Naslov can't be null or empty");
        }
        if (body.getTekst() == null || body.getTekst().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Tekst can't be null or empty");
        }
        if (body.getGroblje() == null) {
            return ResponseEntity.badRequest().body("Groblje can't be null");
        }
        try {
            Obavijest obavijest = obavijestService.save(id, body.getNaslov().trim(), body.getTekst().trim(), body.getSlika().trim(), 
                    body.getGroblje(), "lkristof");
            return ResponseEntity.ok(obavijest);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Obavijest get(@PathVariable Long id) {
        return obavijestService.get(id);
    }

    @GetMapping
    public List<Obavijest> findAll() {
        return obavijestService.findAll();
    }
}
