package hr.fer.opp.gradska_groblja.web.form;

import hr.fer.opp.gradska_groblja.model.enums.Privatnost;

import java.time.LocalDate;

public class LjubimacForm {

    private String ime;

    private String vrsta;

    private LocalDate datumRodenja;

    private LocalDate datumSmrti;

    private String opis;

    private String slika;

    private Privatnost privatnost;

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getVrsta() {
        return vrsta;
    }

    public void setVrsta(String vrsta) {
        this.vrsta = vrsta;
    }

    public LocalDate getDatumRodenja() {
        return datumRodenja;
    }

    public void setDatumRodenja(LocalDate datumRodenja) {
        this.datumRodenja = datumRodenja;
    }

    public LocalDate getDatumSmrti() {
        return datumSmrti;
    }

    public void setDatumSmrti(LocalDate datumSmrti) {
        this.datumSmrti = datumSmrti;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    public Privatnost getPrivatnost() {
        return privatnost;
    }

    public void setPrivatnost(Privatnost privatnost) {
        this.privatnost = privatnost;
    }

}
