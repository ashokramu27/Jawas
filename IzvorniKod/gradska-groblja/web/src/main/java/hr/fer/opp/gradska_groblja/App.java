package hr.fer.opp.gradska_groblja;

import hr.fer.opp.gradska_groblja.web.filter.JWTFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@ComponentScan
public class App {
    public static ConfigurableApplicationContext context;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public FilterRegistrationBean jwtFilter() {
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new JWTFilter());
        
        registrationBean.addUrlPatterns("/api/usluga/add", "/api/usluga/update/*", "/api/ljubimac/add", "/api/ljubimac/update/*");

        return registrationBean;
    }

    public static void main(String[] args) {
        context = SpringApplication.run(App.class, args);
    }
}
