package hr.fer.opp.gradska_groblja.web.controller;

import hr.fer.opp.gradska_groblja.model.GrobnoMjesto;
import hr.fer.opp.gradska_groblja.model.Korisnik;
import hr.fer.opp.gradska_groblja.model.KorisnikUslugaGrobnoMjesto;
import hr.fer.opp.gradska_groblja.model.Usluga;
import hr.fer.opp.gradska_groblja.service.IGrobnoMjestoService;
import hr.fer.opp.gradska_groblja.service.IKorisnikService;
import hr.fer.opp.gradska_groblja.service.IKorisnikUslugaGrobnoMjestoService;
import hr.fer.opp.gradska_groblja.service.IUslugaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/korisnik_usluga_mjesto")
public class KorisnikUslugaGrobnoMjestoController {

    private final IKorisnikUslugaGrobnoMjestoService service;
    private final IKorisnikService korisnikService;
    private final IUslugaService uslugaService;
    private final IGrobnoMjestoService grobnoMjestoService;


    @Autowired
    public KorisnikUslugaGrobnoMjestoController(IKorisnikUslugaGrobnoMjestoService service, IKorisnikService korisnikService,
                                                IUslugaService uslugaService, IGrobnoMjestoService grobnoMjestoService) {
        this.service = service;
        this.korisnikService = korisnikService;
        this.uslugaService = uslugaService;
        this.grobnoMjestoService = grobnoMjestoService;
    }

    @PostMapping
    public ResponseEntity<?> add(Long korisnikId, Long uslugaId, Long grobnoMjestoId, String datumIzvrsenja) {
        if (korisnikId == null) {
            return ResponseEntity.badRequest().body("KorisnikId can't be null or empty");
        }
        Korisnik korisnik = korisnikService.get(korisnikId);
        if (korisnik == null) {
            return ResponseEntity.badRequest().body("Can't find korisnik with given id");
        }

        if (uslugaId == null) {
            return ResponseEntity.badRequest().body("UslugaId can't be null or empty");
        }
        Usluga usluga = uslugaService.get(uslugaId);
        if (usluga == null) {
            return ResponseEntity.badRequest().body("Can't find usluga with given id");
        }

        if (grobnoMjestoId == null) {
            return ResponseEntity.badRequest().body("GrobnoMjestoId can't be null or empty");
        }
        GrobnoMjesto grobnoMjesto = grobnoMjestoService.get(grobnoMjestoId);
        if (grobnoMjesto == null) {
            return ResponseEntity.badRequest().body("Can't find grobno mjesto with given id");
        }

        if(datumIzvrsenja == null) {
            return ResponseEntity.badRequest().body("Datum izvrsenja can't be null or empty");
        }

        try {
            KorisnikUslugaGrobnoMjesto uslugaPoGronomMjestu = service.add(korisnik, usluga, grobnoMjesto, getLocalDateFromString(datumIzvrsenja), "lkristof");
            return ResponseEntity.ok(uslugaPoGronomMjestu);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public KorisnikUslugaGrobnoMjesto get(@PathVariable Long id) {
        return service.get(id);
    }

    @GetMapping
    public List<KorisnikUslugaGrobnoMjesto> findAll() {
        return service.findAll();
    }

    /**
     * @param date must be in format yyyy-MM-dd
     * @return LocalDate object
     * @throws IllegalArgumentException if date format is incorrect
     */
    public LocalDate getLocalDateFromString(String date) {
        Pattern pattern = Pattern.compile("(\\d{4})-(\\d{2})-(\\d{2})");
        Matcher matcher = pattern.matcher(date);
        if (matcher.matches()) {
            return LocalDate.of(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)), Integer.parseInt(matcher.group(3)));
        } else {
            throw new IllegalArgumentException("Date format incorrect");
        }
    }
}
