package hr.fer.opp.gradska_groblja.web.controller;


import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.Krajolik;
import hr.fer.opp.gradska_groblja.service.IGrobljeService;
import hr.fer.opp.gradska_groblja.service.IKrajolikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/krajolik")
public class KrajolikController {
    private final IKrajolikService krajolikService;
    private final IGrobljeService grobljeService;

    @Autowired
    public KrajolikController(IKrajolikService krajolikService, IGrobljeService grobljeService) {
        this.krajolikService = krajolikService;
        this.grobljeService = grobljeService;
    }

    //u postmanu testirate da u body odaberete form-data
    @PostMapping
    public ResponseEntity<?> add(String naziv, String opis, String slika, String lokacija, Long grobljeId) {
        if (naziv == null || naziv.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Naziv can't be null or empty");
        }
        if (lokacija == null || lokacija.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Lokacija can't be null or empty");
        }

        if (grobljeId == null) {
            return ResponseEntity.badRequest().body("GrobljeId can't be null or empty");
        }
        Groblje groblje = grobljeService.get(grobljeId);
        if (groblje == null) {
            return ResponseEntity.badRequest().body("Can't find groblje with given id");
        }
        try {
            Krajolik spomenik =
                    krajolikService.add(naziv.trim(), opis == null ? null : opis.trim(), slika, lokacija.trim(), groblje, "lkristof");
            return ResponseEntity.ok(spomenik);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    //u postmanu testirate da u body stavite raw - json
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Krajolik body) {
        if (body.getNaziv() == null || body.getNaziv().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Naziv can't be null or empty");
        }
        if (body.getLokacija() == null || body.getLokacija().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Lokacija can't be null or empty");
        }

        try {
            Krajolik krajolik = krajolikService.save(id, body.getNaziv().trim(), body.getOpis() == null ? null : body.getOpis().trim(),
                    body.getSlika(), body.getLokacija(), body.getGroblje(), "lkristof");
            return ResponseEntity.ok(krajolik);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Krajolik get(@PathVariable Long id) {
        return krajolikService.get(id);
    }

    @GetMapping
    public List<Krajolik> findAll() {
        return krajolikService.findAll();
    }
}
