package hr.fer.opp.gradska_groblja.web.controller;

import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.GrobnoMjesto;
import hr.fer.opp.gradska_groblja.model.Obavijest;
import hr.fer.opp.gradska_groblja.model.Pokojnik;
import hr.fer.opp.gradska_groblja.service.IGrobljeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/groblje")
public class GrobljeController {
    private final IGrobljeService grobljeService;

    @Autowired
    public GrobljeController(IGrobljeService grobljeService) {
        this.grobljeService = grobljeService;
    }

    //u postmanu testirate da u body odaberete form-data
    @PostMapping
    public ResponseEntity<?> add(String naziv, String adresa, String telefon, String url, String email, String radniDanOd, String radniDanDo,
                                 String vikendOd, String vikendDo, String prijemPokojnikaOd, String prijemPokojnikaDo, String slika, String karta) {
        if (naziv == null || naziv.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Naziv can't be null or empty");
        }
        if (adresa == null || adresa.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Adresa can't be null or empty");
        }
        if (telefon == null || telefon.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Telefon can't be null or empty");
        }
        if (url == null || url.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("URL can't be null or empty");
        }
        if (email == null || email.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("E-mail can't be null or empty");
        }
        if (radniDanOd == null || radniDanOd.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Vrijeme otvaranja na radni dan can't be null or empty");
        }
        if (radniDanDo == null || radniDanDo.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Vrijeme zatvaranja na radni dan can't be null or empty");
        }
        if (vikendOd == null || vikendOd.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Vrijeme otvaranja za vikend can't be null or empty");
        }
        if (vikendDo == null || vikendDo.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Vrijeme zatvaranja za vikend can't be null or empty");
        }
        if (prijemPokojnikaOd == null || prijemPokojnikaOd.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Vrijeme početka prijema pokojnika can't be null or empty");
        }
        if (prijemPokojnikaDo == null || prijemPokojnikaDo.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Vrijeme završetka prijema pokojnik can't be null or empty");
        }
        if (slika == null || slika.trim().isEmpty()) {
            throw new IllegalArgumentException("Slika Groblja (URL) can't be null or empty");
        }
        if (karta == null || karta.trim().isEmpty()) {
            throw new IllegalArgumentException("Karta Groblja (URL) can't be null or empty");
        }
        if (getLocalTimeFromString(radniDanOd).compareTo(getLocalTimeFromString(radniDanDo)) >= 0) {
            return ResponseEntity.badRequest().body("RadniDanDo can`t be smaller than RadniDanOd");
        }
        if (getLocalTimeFromString(vikendOd).compareTo(getLocalTimeFromString(vikendDo)) >= 0) {
            return ResponseEntity.badRequest().body("VikendDo can`t be smaller than VikendOd");
        }
        if (getLocalTimeFromString(prijemPokojnikaOd).compareTo(getLocalTimeFromString(prijemPokojnikaDo)) >= 0) {
            return ResponseEntity.badRequest().body("PrijemPokojnikaDo can`t be smaller than PrijemPokojnikaOd");
        }

        try {
            Groblje groblje = grobljeService.add(naziv.trim(), adresa.trim(), telefon.trim(), url.trim(), email.trim(),
                    getLocalTimeFromString(radniDanOd), getLocalTimeFromString(radniDanDo),
                    getLocalTimeFromString(vikendOd), getLocalTimeFromString(vikendDo),
                    getLocalTimeFromString(prijemPokojnikaOd), getLocalTimeFromString(prijemPokojnikaDo),
                    slika.trim(), karta.trim(), "lkristof");
            return ResponseEntity.ok(groblje);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    //u postmanu testirate da u body stavite raw - json
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Groblje body) {
        if (body.getNaziv() == null || body.getNaziv().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Naziv can't be null or empty");
        }
        if (body.getAdresa() == null || body.getAdresa().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Adresa can't be null or empty");
        }
        if (body.getTelefon() == null || body.getTelefon().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Telefon can't be null or empty");
        }
        if (body.getUrl() == null || body.getUrl().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("URL can't be null or empty");
        }
        if (body.getEmail() == null || body.getEmail().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("E-mail can't be null or empty");
        }
        if (body.getRadniDanOd() == null) {
            return ResponseEntity.badRequest().body("Vrijeme otvaranja na radni dan can't be null or empty");
        }
        if (body.getRadniDanDo() == null) {
            return ResponseEntity.badRequest().body("Vrijeme zatvaranja na radni dan can't be null or empty");
        }
        if (body.getVikendOd() == null) {
            return ResponseEntity.badRequest().body("Vrijeme otvaranja za vikend can't be null or empty");
        }
        if (body.getVikendDo() == null) {
            return ResponseEntity.badRequest().body("Vrijeme zatvaranja za vikend can't be null or empty");
        }
        if (body.getPrijemPokojnikaOd() == null) {
            return ResponseEntity.badRequest().body("Vrijeme početka prijema pokojnika can't be null or empty");
        }
        if (body.getPrijemPokojnikaDo() == null) {
            return ResponseEntity.badRequest().body("Vrijeme završetka prijema pokojnika can't be null or empty");
        }
        if (body.getSlika() == null || body.getSlika().trim().isEmpty()) {
            throw new IllegalArgumentException("Slika Groblja (URL) can't be null or empty");
        }
        if (body.getKarta() == null || body.getKarta().trim().isEmpty()) {
            throw new IllegalArgumentException("Karta Groblja (URL) can't be null or empty");
        }
        if (body.getRadniDanOd().compareTo(body.getRadniDanDo()) >= 0) {
            return ResponseEntity.badRequest().body("RadniDanDo can`t be smaller than RadniDanOd");
        }
        if (body.getVikendOd().compareTo(body.getVikendDo()) >= 0) {
            return ResponseEntity.badRequest().body("VikendDo can`t be smaller than VikendOd");
        }
        if (body.getPrijemPokojnikaOd().compareTo(body.getPrijemPokojnikaDo()) >= 0) {
            return ResponseEntity.badRequest().body("PrijemPokojnikaDo can`t be smaller than PrijemPokojnikaOd");
        }
        try {
            Groblje groblje = grobljeService.save(id, body.getNaziv().trim(), body.getAdresa().trim(), body.getTelefon().trim(),
                    body.getUrl().trim(), body.getEmail().trim(), body.getRadniDanOd(), body.getRadniDanDo(), body.getVikendOd(),
                    body.getVikendDo(), body.getPrijemPokojnikaOd(), body.getPrijemPokojnikaDo(),
                    body.getSlika().trim(), body.getKarta().trim(), "lkristof");
            return ResponseEntity.ok(groblje);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Groblje get(@PathVariable Long id) {
        return grobljeService.get(id);
    }

    @GetMapping("/{id}/pokojnik")
    public List<Pokojnik> getPokojnike(@PathVariable Long id) {
        List<Pokojnik> pokojnici = new ArrayList<>();
        List<GrobnoMjesto> grobnaMjesta = grobljeService.get(id).getListGrobnaMjesta();

        for (GrobnoMjesto grobnoMjesto : grobnaMjesta){
            pokojnici.addAll(grobnoMjesto.getListPokojnici());
        }

        return pokojnici;
    }

    @GetMapping("/{id}/obavijest")
    public List<Obavijest> getObavijesti(@PathVariable Long id) {
        List<Obavijest> obavijesti = new ArrayList<>();
        obavijesti  = grobljeService.get(id).getListObavijesti();

        return obavijesti;
    }

    @GetMapping
    public List<Groblje> findAll() {
        return grobljeService.findAll();
    }

    /**
     * @param time must be in format hh-mm
     * @return LocalDate object
     * @throws IllegalArgumentException if date format is incorrect
     */
    public LocalTime getLocalTimeFromString(String time) {
        Pattern pattern = Pattern.compile("(\\d{2})-(\\d{2})");
        Matcher matcher = pattern.matcher(time);
        if (matcher.matches()) {
            if (Integer.parseInt(matcher.group(1)) > 23) {
                throw new IllegalArgumentException("Hour value must be in range from 00 to 23");
            } else if (Integer.parseInt(matcher.group(2)) > 59) {
                throw new IllegalArgumentException("Minute value must be in range from 00 to 59");
            }
            return LocalTime.of(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)));
        } else {
            throw new IllegalArgumentException("Time format incorrect");
        }
    }
}
