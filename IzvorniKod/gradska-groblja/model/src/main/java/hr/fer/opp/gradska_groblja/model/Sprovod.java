package hr.fer.opp.gradska_groblja.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Sprovod implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDateTime pocetak;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pokojnik_id")
    private Pokojnik pokojnik;

    @Column
    @JsonIgnore
    private LocalDateTime kreirano;

    @Column
    @JsonIgnore
    private String korisnikKreirano;

    @Column
    @JsonIgnore
    private LocalDateTime izmjena;

    @Column
    @JsonIgnore
    private String korisnikIzmjena;

    public Sprovod() {
    }

    public Sprovod(Long id, LocalDateTime pocetak, Pokojnik pokojnik, LocalDateTime kreirano, String korisnikKreirano, LocalDateTime izmjena,
                   String korisnikIzmjena) {
        this.id = id;
        this.pocetak = pocetak;
        this.pokojnik = pokojnik;
        this.kreirano = kreirano;
        this.korisnikKreirano = korisnikKreirano;
        this.izmjena = izmjena;
        this.korisnikIzmjena = korisnikIzmjena;
    }

    public Sprovod(LocalDateTime pocetak, Pokojnik pokojnik, LocalDateTime kreirano, String korisnikKreirano, LocalDateTime izmjena, String korisnikIzmjena) {
        this(null, pocetak, pokojnik, kreirano, korisnikKreirano, izmjena, korisnikIzmjena);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getPocetak() {
        return pocetak;
    }

    public void setPocetak(LocalDateTime pocetak) {
        this.pocetak = pocetak;
    }

    public Pokojnik getPokojnik() {
        return pokojnik;
    }

    public LocalDateTime getKreirano() {
        return kreirano;
    }

    public String getKorisnikKreirano() {
        return korisnikKreirano;
    }

    public LocalDateTime getIzmjena() {
        return izmjena;
    }

    public String getKorisnikIzmjena() {
        return korisnikIzmjena;
    }

    public void setPokojnik(Pokojnik pokojnik) {
        pokojnik.setSprovod(this);
        this.pokojnik = pokojnik;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sprovod sprovod = (Sprovod) o;
        return Objects.equals(id, sprovod.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
