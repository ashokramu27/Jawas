package hr.fer.opp.gradska_groblja.model.enums;

public enum Uloga {
    ADMIN, KORISNIK
}
