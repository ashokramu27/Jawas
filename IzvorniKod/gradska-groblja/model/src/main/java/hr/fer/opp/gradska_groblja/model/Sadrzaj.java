package hr.fer.opp.gradska_groblja.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import hr.fer.opp.gradska_groblja.model.enums.VrstaSadrzaja;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Sadrzaj implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private VrstaSadrzaja vrsta;

    @Column(nullable = false)
    private String lokacija;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "groblje_id")
    @JsonIgnore
    private Groblje groblje;

    @Column
    @JsonIgnore
    private LocalDateTime kreirano;

    @Column
    @JsonIgnore
    private String korisnikKreirano;

    @Column
    @JsonIgnore
    private LocalDateTime izmjena;

    @Column
    @JsonIgnore
    private String korisnikIzmjena;

    public Sadrzaj() {
    }

    public Sadrzaj(Long id, VrstaSadrzaja vrsta, String lokacija, Groblje groblje, LocalDateTime kreirano, String korisnikKreirano,
                   LocalDateTime izmjena, String korisnikIzmjena) {
        this.id = id;
        this.vrsta = vrsta;
        this.lokacija = lokacija;
        this.groblje = groblje;
        this.kreirano = kreirano;
        this.korisnikKreirano = korisnikKreirano;
        this.izmjena = izmjena;
        this.korisnikIzmjena = korisnikIzmjena;
    }

    public Sadrzaj(VrstaSadrzaja vrsta, String lokacija, Groblje groblje, LocalDateTime kreirano, String korisnikKreirano,
                   LocalDateTime izmjena, String korisnikIzmjena) {
        this(null, vrsta, lokacija, groblje, kreirano, korisnikKreirano, izmjena, korisnikIzmjena);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VrstaSadrzaja getVrsta() {
        return vrsta;
    }

    public void setVrsta(VrstaSadrzaja vrsta) {
        this.vrsta = vrsta;
    }

    public String getLokacija() {
        return lokacija;
    }

    public void setLokacija(String lokacija) {
        this.lokacija = lokacija;
    }

    public Groblje getGroblje() {
        return groblje;
    }

    public void setGroblje(Groblje groblje) {
        this.groblje = groblje;
    }

    public LocalDateTime getKreirano() {
        return kreirano;
    }

    public String getKorisnikKreirano() {
        return korisnikKreirano;
    }

    public LocalDateTime getIzmjena() {
        return izmjena;
    }

    public String getKorisnikIzmjena() {
        return korisnikIzmjena;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sadrzaj sadrzaj = (Sadrzaj) o;
        return Objects.equals(id, sadrzaj.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
