package hr.fer.opp.gradska_groblja.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Krajolik implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String naziv;

    @Column
    private String opis;

    @Column
    private String slika;

    @Column(nullable = false)
    private String lokacija;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "groblje_id")
    @JsonIgnore
    private Groblje groblje;

    @Column
    @JsonIgnore
    private LocalDateTime kreirano;

    @Column
    @JsonIgnore
    private String korisnikKreirano;

    @Column
    @JsonIgnore
    private LocalDateTime izmjena;

    @Column
    @JsonIgnore
    private String korisnikIzmjena;

    public Krajolik() {
    }

    public Krajolik(Long id, String naziv, String opis, String slika, String lokacija, Groblje groblje, LocalDateTime kreirano, String korisnikKreirano,
                    LocalDateTime izmjena, String korisnikIzmjena) {
        this.id = id;
        this.naziv = naziv;
        this.opis = opis;
        this.slika = slika;
        this.lokacija = lokacija;
        this.groblje = groblje;
        this.kreirano = kreirano;
        this.korisnikKreirano = korisnikKreirano;
        this.izmjena = izmjena;
        this.korisnikIzmjena = korisnikIzmjena;
    }

    public Krajolik(String naziv, String opis, String slika, String lokacija, Groblje groblje, LocalDateTime kreirano, String korisnikKreirano,
                    LocalDateTime izmjena, String korisnikIzmjena) {
        this(null, naziv, opis, slika, lokacija, groblje, kreirano, korisnikKreirano,
                izmjena, korisnikIzmjena);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    public String getLokacija() {
        return lokacija;
    }

    public void setLokacija(String lokacija) {
        this.lokacija = lokacija;
    }

    public Groblje getGroblje() {
        return groblje;
    }

    public void setGroblje(Groblje groblje) {
        this.groblje = groblje;
    }

    public LocalDateTime getKreirano() {
        return kreirano;
    }

    public String getKorisnikKreirano() {
        return korisnikKreirano;
    }

    public LocalDateTime getIzmjena() {
        return izmjena;
    }

    public String getKorisnikIzmjena() {
        return korisnikIzmjena;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Krajolik krajolik = (Krajolik) o;
        return Objects.equals(id, krajolik.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
