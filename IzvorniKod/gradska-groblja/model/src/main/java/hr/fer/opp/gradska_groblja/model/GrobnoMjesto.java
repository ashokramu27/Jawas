package hr.fer.opp.gradska_groblja.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class GrobnoMjesto implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String odjel;

    @Column
    private Short polje;

    @Column
    private Short razred;

    @Column(nullable = false)
    private Integer broj;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "groblje_id")
    private Groblje groblje;

    @OneToMany(mappedBy = "grobnoMjesto")
    @JsonIgnore
    private List<Pokojnik> listPokojnici;

    @ManyToMany(mappedBy = "listGrobnaMjesta")
    @JsonIgnore
    private List<Korisnik> vlasnici;

    @OneToMany(mappedBy = "grobnoMjesto")
    @JsonIgnore
    private List<KorisnikUslugaGrobnoMjesto> uslugaPoGrobnomMjestu;

    @Column
    @JsonIgnore
    private LocalDateTime kreirano;

    @Column
    @JsonIgnore
    private String korisnikKreirano;

    @Column
    @JsonIgnore
    private LocalDateTime izmjena;

    @Column
    @JsonIgnore
    private String korisnikIzmjena;

    public GrobnoMjesto() {
    }

    public GrobnoMjesto(Long id, String odjel, Short polje, Short razred, Integer broj, Groblje groblje, LocalDateTime kreirano, String korisnikKreirano,
                        LocalDateTime izmjena, String korisnikIzmjena) {
        this.id = id;
        this.odjel = odjel;
        this.polje = polje;
        this.razred = razred;
        this.broj = broj;
        this.groblje = groblje;
        this.listPokojnici = new LinkedList<>();
        this.vlasnici = new LinkedList<>();
        this.uslugaPoGrobnomMjestu = new LinkedList<>();
        this.kreirano = kreirano;
        this.korisnikKreirano = korisnikKreirano;
        this.izmjena = izmjena;
        this.korisnikIzmjena = korisnikIzmjena;
    }

    public GrobnoMjesto(String odjel, Short polje, Short razred, Integer broj, Groblje groblje, LocalDateTime kreirano, String korisnikKreirano,
                        LocalDateTime izmjena, String korisnikIzmjena) {
        this(null, odjel, polje, razred, broj, groblje, kreirano, korisnikKreirano,
                izmjena, korisnikIzmjena);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOdjel() {
        return odjel;
    }

    public void setOdjel(String odjel) {
        this.odjel = odjel;
    }

    public Short getPolje() {
        return polje;
    }

    public void setPolje(Short polje) {
        this.polje = polje;
    }

    public Short getRazred() {
        return razred;
    }

    public void setRazred(Short razred) {
        this.razred = razred;
    }

    public Integer getBroj() {
        return broj;
    }

    public void setBroj(Integer broj) {
        this.broj = broj;
    }

    public Groblje getGroblje() {
        return groblje;
    }

    public void setGroblje(Groblje groblje) {
        this.groblje = groblje;
    }

    public List<Pokojnik> getListPokojnici() {
        return listPokojnici;
    }

    public void setListPokojnici(List<Pokojnik> listPokojnici) {
        this.listPokojnici = listPokojnici;
    }

    public LocalDateTime getKreirano() {
        return kreirano;
    }

    public String getKorisnikKreirano() {
        return korisnikKreirano;
    }

    public LocalDateTime getIzmjena() {
        return izmjena;
    }

    public String getKorisnikIzmjena() {
        return korisnikIzmjena;
    }

    public void addPokojnik(Pokojnik pokojnik) {
        pokojnik.setGrobnoMjesto(this);
        this.listPokojnici.add(pokojnik);
    }

    public List<Korisnik> getVlasnici() {
        return vlasnici;
    }

    public void setVlasnici(List<Korisnik> vlasnici) {
        this.vlasnici = vlasnici;
    }

    public void addVlasnik(Korisnik korisnik) {
        vlasnici.add(korisnik);
    }

    public List<KorisnikUslugaGrobnoMjesto> getUslugaPoGrobnomMjestu() {
        return uslugaPoGrobnomMjestu;
    }

    public void setUslugaPoGrobnomMjestu(List<KorisnikUslugaGrobnoMjesto> uslugaPoGrobnomMjestu) {
        this.uslugaPoGrobnomMjestu = uslugaPoGrobnomMjestu;
    }

    public void addUsluga(KorisnikUslugaGrobnoMjesto usluga){
        usluga.setGrobnoMjesto(this);
        this.uslugaPoGrobnomMjestu.add(usluga);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GrobnoMjesto that = (GrobnoMjesto) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
