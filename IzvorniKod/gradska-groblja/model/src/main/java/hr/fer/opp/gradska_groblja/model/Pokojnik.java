package hr.fer.opp.gradska_groblja.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Inheritance(strategy = InheritanceType.JOINED)
public class Pokojnik implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String ime;

    @Column(nullable = false)
    private String prezime;

    @Column
    private LocalDate datumRodenja;

    @Column
    private LocalDate datumSmrti;

    @Column
    private LocalDate datumUkopa;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grobno_mjesto_id")
    private GrobnoMjesto grobnoMjesto;

    @Transient
    private String grobnoMjestoString;

    @OneToOne(mappedBy = "pokojnik")
    @JsonIgnore
    private Sprovod sprovod;

    @Transient
    private String sprovodStartDateTime;

    @Column
    @JsonIgnore
    private LocalDateTime kreirano;

    @Column
    @JsonIgnore
    private String korisnikKreirano;

    @Column
    @JsonIgnore
    private LocalDateTime izmjena;

    @Column
    @JsonIgnore
    private String korisnikIzmjena;

    public Pokojnik() {
    }

    public Pokojnik(Long id, String ime, String prezime, LocalDate datumRodenja, LocalDate datumSmrti, LocalDate datumUkopa,
                    GrobnoMjesto grobnoMjesto, LocalDateTime kreirano, String korisnikKreirano,
                    LocalDateTime izmjena, String korisnikIzmjena) {
        this.id = id;
        this.ime = ime;
        this.prezime = prezime;
        this.datumRodenja = datumRodenja;
        this.datumSmrti = datumSmrti;
        this.datumUkopa = datumUkopa;
        this.grobnoMjesto = grobnoMjesto;
        this.grobnoMjestoString = extractGrobnoMjestoString();
        this.kreirano = kreirano;
        this.korisnikKreirano = korisnikKreirano;
        this.izmjena = izmjena;
        this.korisnikIzmjena = korisnikIzmjena;
    }

    public Pokojnik(String ime, String prezime, LocalDate datumRodenja, LocalDate datumSmrti, LocalDate datumUkopa, GrobnoMjesto grobnoMjesto,
                    LocalDateTime kreirano, String korisnikKreirano, LocalDateTime izmjena, String korisnikIzmjena) {
        this(null, ime, prezime, datumRodenja, datumSmrti, datumUkopa, grobnoMjesto, kreirano, korisnikKreirano,
                izmjena, korisnikIzmjena);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public LocalDate getDatumRodenja() {
        return datumRodenja;
    }

    public void setDatumRodenja(LocalDate datumRodenja) {
        this.datumRodenja = datumRodenja;
    }

    public LocalDate getDatumSmrti() {
        return datumSmrti;
    }

    public void setDatumSmrti(LocalDate datumSmrti) {
        this.datumSmrti = datumSmrti;
    }

    public LocalDate getDatumUkopa() {
        return datumUkopa;
    }

    public void setDatumUkopa(LocalDate datumUkopa) {
        this.datumUkopa = datumUkopa;
    }

    public GrobnoMjesto getGrobnoMjesto() {
        return grobnoMjesto;
    }

    public void setGrobnoMjesto(GrobnoMjesto grobnoMjesto) {
        this.grobnoMjesto = grobnoMjesto;
    }

    public LocalDateTime getKreirano() {
        return kreirano;
    }

    public String getKorisnikKreirano() {
        return korisnikKreirano;
    }

    public LocalDateTime getIzmjena() {
        return izmjena;
    }

    public String getKorisnikIzmjena() {
        return korisnikIzmjena;
    }

    public String getGrobnoMjestoString() {
        return grobnoMjestoString == null ? extractGrobnoMjestoString() : grobnoMjestoString;
    }

    private String extractGrobnoMjestoString() {
        return grobnoMjesto.getOdjel() + "-" + grobnoMjesto.getPolje() + "-" + grobnoMjesto.getRazred() + "-" + grobnoMjesto.getBroj();
    }

    public Sprovod getSprovod() {
        return sprovod;
    }

    public void setSprovod(Sprovod sprovod) {
        this.sprovod = sprovod;
    }

    public String getSprovodStartDateTime() {
        return sprovodStartDateTime == null ? extractSprovodStartDateTime() : sprovodStartDateTime;
    }

    private String extractSprovodStartDateTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy, HH-mm");
        return sprovod == null ? null : sprovod.getPocetak().format(formatter);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pokojnik that = (Pokojnik) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
