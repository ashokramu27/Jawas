package hr.fer.opp.gradska_groblja.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
public class KorisnikUslugaGrobnoMjesto implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "korisnik_id")
    private Korisnik korisnik;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usluga_id")
    private Usluga usluga;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grobno_mjesto_id")
    private GrobnoMjesto grobnoMjesto;

    @Column(nullable = false)
    private LocalDate datumIzvrsenja;

    @Column
    @JsonIgnore
    private LocalDateTime kreirano;

    @Column
    @JsonIgnore
    private String korisnikKreirano;

    @Column
    @JsonIgnore
    private LocalDateTime izmjena;

    @Column
    @JsonIgnore
    private String korisnikIzmjena;

    public KorisnikUslugaGrobnoMjesto() {

    }

    public KorisnikUslugaGrobnoMjesto(Long id, Korisnik korisnik, Usluga usluga, GrobnoMjesto grobnoMjesto, LocalDate datumIzvrsenja, LocalDateTime kreirano, String korisnikKreirano,
                                      LocalDateTime izmjena, String korisnikIzmjena) {
        this.korisnik = korisnik;
        this.usluga = usluga;
        this.grobnoMjesto = grobnoMjesto;
        this.datumIzvrsenja = datumIzvrsenja;
        this.kreirano = kreirano;
        this.korisnikKreirano = korisnikKreirano;
        this.izmjena = izmjena;
        this.korisnikIzmjena = korisnikIzmjena;
    }

    public KorisnikUslugaGrobnoMjesto(Korisnik korisnik, Usluga usluga, GrobnoMjesto grobnoMjesto, LocalDate datumIzvrsenja, LocalDateTime kreirano,
                                      String korisnikKreirano, LocalDateTime izmjena, String korisnikIzmjena) {
        this(null, korisnik, usluga, grobnoMjesto, datumIzvrsenja, kreirano, korisnikKreirano,
                izmjena, korisnikIzmjena);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public Usluga getUsluga() {
        return usluga;
    }

    public void setUsluga(Usluga usluga) {
        this.usluga = usluga;
    }

    public GrobnoMjesto getGrobnoMjesto() {
        return grobnoMjesto;
    }

    public void setGrobnoMjesto(GrobnoMjesto grobnoMjesto) {
        this.grobnoMjesto = grobnoMjesto;
    }

    public LocalDate getDatumIzvrsenja() {
        return datumIzvrsenja;
    }

    public void setDatumIzvrsenja(LocalDate datumIzvrsenja) {
        this.datumIzvrsenja = datumIzvrsenja;
    }

    public LocalDateTime getKreirano() {
        return kreirano;
    }

    public String getKorisnikKreirano() {
        return korisnikKreirano;
    }

    public LocalDateTime getIzmjena() {
        return izmjena;
    }

    public String getKorisnikIzmjena() {
        return korisnikIzmjena;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KorisnikUslugaGrobnoMjesto that = (KorisnikUslugaGrobnoMjesto) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
