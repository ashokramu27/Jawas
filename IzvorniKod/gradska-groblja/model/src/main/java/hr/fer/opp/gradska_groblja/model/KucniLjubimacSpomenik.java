package hr.fer.opp.gradska_groblja.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import hr.fer.opp.gradska_groblja.model.enums.Privatnost;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class KucniLjubimacSpomenik implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String ime;

    @Column(nullable = false)
    private String vrsta;

    @Column
    private LocalDate datumRodenja;

    @Column
    private LocalDate datumSmrti;

    @Column
    private String opis;

    @Column
    private String slika;

    @Column(nullable = false)
    @ColumnDefault("'JAVNO'")
    @Enumerated(EnumType.STRING)
    private Privatnost privatnost;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "korisnik_id")
    @JsonIgnore
    private Korisnik korisnik;

    @Transient
    private String vlasnikImePrezime;

    @Column
    @JsonIgnore
    private LocalDateTime kreirano;

    @Column
    @JsonIgnore
    private String korisnikKreirano;

    @Column
    @JsonIgnore
    private LocalDateTime izmjena;

    @Column
    @JsonIgnore
    private String korisnikIzmjena;

    public KucniLjubimacSpomenik() {
    }

    public KucniLjubimacSpomenik(Long id, String ime, String vrsta, LocalDate datumRodenja, LocalDate datumSmrti, String opis,
                                 String slika, Privatnost privatnost, Korisnik korisnik, LocalDateTime kreirano, String korisnikKreirano,
                                 LocalDateTime izmjena, String korisnikIzmjena) {
        this.id = id;
        this.ime = ime;
        this.vrsta = vrsta;
        this.datumRodenja = datumRodenja;
        this.datumSmrti = datumSmrti;
        this.opis = opis;
        this.slika = slika;
        this.privatnost = privatnost;
        this.korisnik = korisnik;
        this.vlasnikImePrezime = extractVlasnikImePrezime();
        this.kreirano = kreirano;
        this.korisnikKreirano = korisnikKreirano;
        this.izmjena = izmjena;
        this.korisnikIzmjena = korisnikIzmjena;
    }

    public KucniLjubimacSpomenik(String ime, String vrsta, LocalDate datumRodenja, LocalDate datumSmrti, String opis,
                                 String slika, Privatnost privatnost, Korisnik korisnik, LocalDateTime kreirano, String korisnikKreirano,
                                 LocalDateTime izmjena, String korisnikIzmjena) {
        this(null, ime, vrsta, datumRodenja, datumSmrti, opis, slika, privatnost, korisnik, kreirano, korisnikKreirano,
                izmjena, korisnikIzmjena);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getVrsta() {
        return vrsta;
    }

    public void setVrsta(String vrsta) {
        this.vrsta = vrsta;
    }

    public LocalDate getDatumRodenja() {
        return datumRodenja;
    }

    public void setDatumRodenja(LocalDate datumRodenja) {
        this.datumRodenja = datumRodenja;
    }

    public LocalDate getDatumSmrti() {
        return datumSmrti;
    }

    public void setDatumSmrti(LocalDate datumSmrti) {
        this.datumSmrti = datumSmrti;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    public Privatnost getPrivatnost() {
        return privatnost;
    }

    public void setPrivatnost(Privatnost privatnost) {
        this.privatnost = privatnost;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public String getVlasnikImePrezime() {
        return vlasnikImePrezime == null ? extractVlasnikImePrezime() : this.vlasnikImePrezime;
    }

    public void setVlasnikImePrezime(String vlasnikImePrezime) {
        this.vlasnikImePrezime = vlasnikImePrezime;
    }

    private String extractVlasnikImePrezime() {
        return korisnik.getIme() + " " + korisnik.getPrezime();
    }

    public LocalDateTime getKreirano() {
        return kreirano;
    }

    public String getKorisnikKreirano() {
        return korisnikKreirano;
    }

    public LocalDateTime getIzmjena() {
        return izmjena;
    }

    public String getKorisnikIzmjena() {
        return korisnikIzmjena;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KucniLjubimacSpomenik that = (KucniLjubimacSpomenik) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
