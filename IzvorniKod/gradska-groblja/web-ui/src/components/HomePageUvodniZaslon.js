import React from "react";
import './HomePageUvodniZaslon.css';
import {Link} from 'react-router-dom';
import paths from "../constants/paths";


const HomePageUvodniZaslon = () => {

    return (
        <div id="att-image">
            <div className="overlay"/>
            <div className="intro">
                <h1>
                    <Link to={paths.routes.grobljeList}>Gradska groblja </Link>
                    nisu samo posljednje počivalište naših pokojnih već su i
                    <Link to={paths.routes.krajolikList}> muzeji na otvorenom </Link>
                    koji očaravaju svojim mirom i ljepotom te pružaju mogućnost obilježavanja života i smrti Vaših
                    <Link to={paths.routes.ljubimacList}> kućnih ljubimaca</Link>
                </h1>
            </div>

        </div>
    );
};

export default HomePageUvodniZaslon;
