import React from "react";
import "./GrobnoMjesto.css";

const GrobnoMjesto = (props) => {
    return (
        <div>
            <li>{props.grobnoMjesto.odjel},
                {props.grobnoMjesto.polje},
                {props.grobnoMjesto.razred},
                {props.grobnoMjesto.broj}</li>
            <br/>
        </div>
    );
};

export default GrobnoMjesto;