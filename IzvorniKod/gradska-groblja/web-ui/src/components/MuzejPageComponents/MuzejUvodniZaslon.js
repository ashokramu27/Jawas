import React from "react";
import './MuzejUvodniZaslon.css';
import {Link} from "react-router-dom";
import paths from "../../constants/paths";


const MuzejUvodniZaslon = (props) => {

    const backStyle = {
        backgroundImage: 'url(' + props.groblje.slika + ')',
    };

    return (
        <div id="krajolik-uvodni" style={backStyle}>
            <div className="intro">
                <h1>Muzej</h1>
                <h2><Link className="no-style-Link"
                          to={paths.routes.groblje(props.groblje.id)}>{props.groblje.naziv}</Link></h2>
            </div>
            <div className="overlay"/>
            <div className="banner"/>
        </div>
    );
};

export default MuzejUvodniZaslon;
