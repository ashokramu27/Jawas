import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import './KorisnikoviLjubimci.css';
import Modal from "react-bootstrap/es/Modal";
import ljubimacActionCreator from "../../actionCreators/ljubimacActionCreator";
import Ljubimac from "./Ljubimac";
import {getCurrentDate} from "../../utils/helpFunctions";
import moment from "moment";

class KorisnkoviLjubimci extends Component {

    constructor(props) {
        super(props);

        this.state = {
            modalIsOpen: false,
            disableUcitaj: true,
            datRodErr: false,
            datSmrtiErr: false,
        };

        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.uploadImage = this.uploadImage.bind(this);
        this.validate = this.validate.bind(this);
        this.addPet = this.addPet.bind(this);
        this.checkIfNull = this.checkIfNull.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }

    handleChange() {
        this.setState({disableUcitaj: this.refs.slika.value.toString().trim() === ""})
    }

    uploadImage(e) {
        e.preventDefault();
        document.getElementById("upload").src = this.refs.slika.value.toString().trim();
    }


    validate(e) {

        e.preventDefault();

        let errors = 0;
        errors = this.checkIfNull(this.refs.ime, "ime", errors);
        errors = this.checkIfNull(this.refs.vrsta, "vrsta", errors);

        //check date format
        let currentDate = getCurrentDate();
        let errDate = false;
        if (this.refs.datRodenja.value
            && (!moment(this.refs.datRodenja.value.toString().trim(), "YYYY-MM-DD", true).isValid()
                || this.refs.datRodenja.value.toString().trim() > currentDate)) {
            errDate = true;
            this.setState({datRodErr: true});
        } else {
            this.setState({datRodErr: false});
        }

        if (this.refs.datSmrti.value
            && (!moment(this.refs.datSmrti.value.toString().trim(), "YYYY-MM-DD", true).isValid()
                || this.refs.datSmrti.value.toString().trim() > currentDate)) {
            errDate = true;
            this.setState({datSmrtiErr: true});
        } else {
            this.setState({datSmrtiErr: false});
        }

        if (this.refs.datRodenja.value && this.refs.datSmrti.value && !errDate
            && (this.refs.datRodenja.value.toString().trim() > this.refs.datSmrti.value.toString().trim())) {
            errors++;
            this.setState({datRodErr: true, datSmrtiErr: true});
        }

        if (!errors) this.addPet();

    }

    checkIfNull(el, id, errors) {
        if (!el.value) {
            document.getElementById(id).className += " required-field";
            return ++errors;
        }
        return errors;
    }

    addPet() {

        this.props.dispatch(ljubimacActionCreator.postLjubimac({
            ime: this.refs.ime.value.toString().trim(),
            vrsta: this.refs.vrsta.value.toString().trim(),
            datumRodenja: this.refs.datRodenja.value.toString().trim(),
            datumSmrti: this.refs.datSmrti.value.toString().trim(),
            opis: this.refs.opis.value.toString().trim(),
            slika: this.refs.slika.value.toString().trim(),
            privatnost: document.querySelector('input[name="privatnost"]:checked').value
        }));

        this.closeModal();
    }


    render() {

        let ljubimci = [];

        if (this.props.korisnik && this.props.ljubimci.all) {
            let vlasnik = this.props.korisnik.ime + " " + this.props.korisnik.prezime;
            ljubimci = this.props.ljubimci.all
                .filter(x => x.vlasnikImePrezime === vlasnik)
                .map(x => <Ljubimac key={x.id} ljubimac={x}/>);
        }

        return (
            <div>
                <div className="header-image">
                    <div className="intro">
                        <h1>Ljubimci</h1>
                    </div>
                </div>
                {this.props.loggedIn ?
                    <div id="pets" className="container">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="ljubimci-header">
                                    <h1>Moji kućni ljubimci</h1>
                                    {this.props.loggedIn && (
                                        <button className="btn-more" onClick={this.openModal}>Novi ljubimac
                                            <i className="fa fa-plus" aria-hidden="true"/>
                                        </button>
                                    )}
                                </div>
                            </div>
                        </div>
                        <div className="row flex-row ">
                            {ljubimci}
                        </div>
                    </div>
                    : <div/>
                }

                <Modal show={this.state.modalIsOpen} onHide={this.closeModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            Dodaj novog ljubimca
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="modalWrap">
                            <form>
                                <div className="form-row">
                                    <div className="form-group col-md-12">
                                        <label>Ime</label>
                                        <input id="ime" className="form-control" placeholder="Ime" ref="ime"/>
                                    </div>
                                    <div className="form-group col-md-12">
                                        <label>Vrsta</label>
                                        <input id="vrsta" className="form-control" placeholder="Vrsta" ref="vrsta"/>
                                    </div>
                                </div>
                                <div className="form-row">
                                    <div className="form-group col-md-6">
                                        <label>Datum rođenja</label>
                                        <input id="datRodenja"
                                               className={this.state.datRodErr ? "form-control required-field" : "form-control"}
                                               placeholder="yyyy-MM-dd"
                                               ref="datRodenja"/>
                                    </div>
                                    <div className="form-group col-md-6">
                                        <label>Datum smrti</label>
                                        <input id="datSmrti"
                                               className={this.state.datSmrtiErr ? "form-control required-field" : "form-control"}
                                               placeholder="yyyy-MM-dd"
                                               ref="datSmrti"/>
                                    </div>
                                </div>

                                <div className="form-row">
                                    <div className="form-group col-md-12">
                                        <label>Slika</label>
                                    </div>
                                    <div className="form-group col-md-12 image-upload">
                                        <input id="slika" className="form-control" onChange={this.handleChange}
                                               placeholder="URL slike" ref="slika"/>
                                        <button id="upload-btn" disabled={this.state.disableUcitaj}
                                                onClick={(e) => this.uploadImage(e)}>Učitaj
                                        </button>
                                    </div>
                                </div>
                                <div className="form-row">
                                    <div className="form-group col-md-12">
                                        <img id="upload"/>
                                    </div>
                                </div>
                                <div className="form-row">
                                    <div className="form-group col-md-12">
                                        <label>Opis</label>
                                        <textarea className="form-control" placeholder="Opis" ref="opis"/>
                                    </div>
                                </div>
                                <div className="form-row">
                                    <div className="form-group col-md-12 checkbox-row">
                                        <div className="checkbox-col">
                                            <input type="radio" name="privatnost" value="JAVNO" defaultChecked/> Javno
                                        </div>
                                        <div className="checkbox-col">
                                            <input type="radio" name="privatnost" value="PRIVATNO"/> Privatno
                                        </div>
                                        <div className="checkbox-col">
                                            <input type="radio" name="privatnost" value="SAMO_VLASNIK"/> Samo vlasnik
                                        </div>
                                    </div>
                                </div>
                                <div className="form-row">
                                    <div className="button-wrapper col-md-12">
                                        <button type="submit" className="btn btn-more"
                                                onClick={(e) => this.validate(e)}>Kreiraj
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {loggedIn: !!state.session.token, korisnik: state.session.korisnik, ljubimci: state.ljubimci};
};

export default connect(mapStateToProps)(KorisnkoviLjubimci);
