import React from 'react';
import './OstaliLjubimci.css';

const OstaliLjubimci = (props) => {
    return (
        <div className="ostaliLjubimci">

            <div className="container">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="ljubimci-header">
                            {props.isLoggedIn ? <h1>Ostali kućni ljubimci</h1> : <div/>}
                        </div>
                    </div>
                </div>
                <div className="row flex-row ">
                    {props.list}
                </div>
            </div>

        </div>
    )
};


export default OstaliLjubimci;
