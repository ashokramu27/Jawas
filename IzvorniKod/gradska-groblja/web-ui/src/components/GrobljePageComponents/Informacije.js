import React from 'react';
import './Informacije.css';
import paths from "../../constants/paths";
import {Link} from 'react-router-dom';

const Informacije = (props) => {

    return (
        <div className="container">
            <div className="row">
                <div className="col-md-12 heading text-center"><h2>Informacije</h2></div>
            </div>
            <div className="row informacije">
                <div className="col-sm-6 info-col">
                    <h3>Osnovni podaci</h3>
                    <div>
                        <label>Telefon</label>
                        <h4>{props.groblje.telefon}</h4>
                    </div>
                    <div>
                        <label>Email</label>
                        <h4><a href={"mailto:" + props.groblje.email}>{props.groblje.email}</a></h4>
                    </div>
                    <div>
                        <label>Web adresa</label>
                        <h4><a href={"http://" + props.groblje.url} target="_blank">{props.groblje.url}</a></h4>
                    </div>
                </div>
                <div className="col-sm-6 info-col">
                    <h3>Radno vrijeme</h3>
                    <div>
                        <label>Radni dani</label>
                        <h4>{props.groblje.radniDanOd} - {props.groblje.radniDanDo}</h4>
                    </div>
                    <div>
                        <label>Vikend</label>
                        <h4>{props.groblje.vikendOd} - {props.groblje.vikendDo}</h4>
                    </div>
                    <div>
                        <label>Prijem pokojnika</label>
                        <h4>{props.groblje.prijemPokojnikaOd} - {props.groblje.prijemPokojnikaDo}</h4>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-12 museum-link text-center">
                    Groblja su predivni parkovi i umjetničke galerije. Otvorite
                    poveznicu na <Link to={paths.routes.krajolik(props.groblje.id)}>muzej</Link>
                </div>
            </div>
        </div>
    );

};

export default Informacije;
