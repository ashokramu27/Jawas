import React from "react";
import paths from "../constants/paths";
import {Link} from "react-router-dom";
import "./ErrorPage.css"

const ErrorPage = (props) => (
    <div className="errorPage">
        <h1>{props.errorNumber == null ? "404" : props.errorNumber}</h1>
        <p>{props.errorMessage == null ? "Page not found" : props.errorMessage}</p>
        <Link to={paths.routes.pocetna}>Home page</Link>
    </div>
);

export default ErrorPage;

