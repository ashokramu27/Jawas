import React, {Component} from "react";
import './Header.css';
import {Link} from 'react-router-dom';
import paths from "../constants/paths";
import $ from 'jquery';
import {connect} from "react-redux";

class Header extends Component {

    constructor(props) {
        super(props);
        this.toggleOpen = this.toggleOpen.bind(this);
    }

    toggleOpen() {
        $('#menu-wrap').toggleClass('open');
        $('#menuToggle').toggleClass('open');
    };

    render() {
        return (

            <header id="header-section" role="header">
                <div className="container">
                    <h1 id="logo"><Link to={paths.routes.pocetna}>JAWAS</Link></h1>
                    <div id="menuToggle" onClick={this.toggleOpen}>
                        <span/>
                        <span/>
                        <span/>
                    </div>

                    <nav id="menu-wrap" role="navigation">
                        <ul className="menu">
                            <li> <Link to={paths.routes.grobljeList}>Groblja</Link></li>
                            <li><Link to={paths.routes.krajolikList}>Muzeji</Link></li>
                            <li><Link to={paths.routes.ljubimacList}>Ljubimci</Link></li>
                            <li>
                                {this.props.loggedIn ?
                                    <Link to={paths.routes.odjava}>Odjava</Link>
                                    : <Link to={paths.routes.prijava}>Prijava</Link>}
                            </li>
                            {this.props.loggedIn && this.props.korisnik.uloga === "ADMIN" ?
                                <li>

                                    <Link to={paths.routes.admin}>Admin</Link>
                                </li>
                                : ""
                            }
                            {this.props.loggedIn ?
                                <li style={{fontWeight: 700}}>
                                    <span className="glyphicon glyphicon-user"/>
                                    {this.props.korisnik.korisnickoIme}
                                </li>
                                : <div/>
                            }
                        </ul>
                    </nav>
                </div>
            </header>
        );
    };
}

const mapStateToProps = (state) => {
    return {loggedIn: !!state.session.token, korisnik: state.session.korisnik};
};

export default connect(mapStateToProps)(Header);