import React, {Component} from "react";
import {connect} from "react-redux";
import Sadrzaj from "./../components/Sadrzaj";
import sadrzajActionCreator from "./../actionCreators/sadrzajActionCreator";

class SadrzajList extends Component {
    constructor(props) {
        super(props);
        this.props.dispatch(sadrzajActionCreator.fetchSadrzaje());
    }

    render() {
        let sadrzaji = [];
        if (this.props.sadrzaji) {
            sadrzaji = this.props.sadrzaji.all.map((x) => <Sadrzaj key={x.id} sadrzaj={x}/>);
        }

        return (
            <div>
                <h1>Sadrzaj</h1>
                <ul>
                    {sadrzaji}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {sadrzaji: state.sadrzaji}
};

export default connect(mapStateToProps)(SadrzajList);