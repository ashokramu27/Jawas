import React, {Component} from "react";
import {connect} from "react-redux";
import Sprovod from "./../components/Sprovod.js";
import sprovodActionCreator from "./../actionCreators/sprovodActionCreator";

class SprovodList extends Component {
    constructor(props) {
        super(props);
        this.props.dispatch(sprovodActionCreator.fetchSprovode());
    }

    render() {
        let sprovodi = [];
        if (this.props.sprovodi) {
            sprovodi = this.props.sprovodi.all.map((x) => <Sprovod key={x.id} sprovod={x}/>);
        }

        return (
            <div>
                <h1>Sprovodi</h1>
                <ul>
                    {sprovodi}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {sprovodi: state.sprovodi}
};

export default connect(mapStateToProps)(SprovodList);