import React, {Component} from "react";
import {connect} from "react-redux";
import Licnost from "./../components/Licnost";
import licnostActionCreator from "./../actionCreators/licnostActionCreator";

class LicnostList extends Component {
    constructor(props) {
        super(props);
        this.props.dispatch(licnostActionCreator.fetchLicnosti());
    }

    render() {
        let licnosti = [];
        if (this.props.licnosti) {
            licnosti = this.props.licnosti.all.map((x) => <Licnost key={x.id} licnost={x}/>);
        }

        return (
            <div>
                <h1>Licnosti</h1>
                <ul>
                    {licnosti}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {licnosti: state.licnosti}
};

export default connect(mapStateToProps)(LicnostList);