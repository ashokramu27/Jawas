import React, {Component} from "react";
import './GrobljePage.css';
import Header from '../components/Header';
import UvodniZaslon from '../components/GrobljePageComponents/UvodniZaslon';
import Obavijesti from '../components/GrobljePageComponents/Obavijesti';
import Karta from '../components/GrobljePageComponents/Karta';
import Informacije from '../components/GrobljePageComponents/Informacije';
import Footer from '../components/Footer';
import {connect} from "react-redux";
import grobljeActionCreator from "../actionCreators/grobljeActionCreator";
import ErrorPage from "../components/ErrorPage";

class GrobljePage extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        let grobljeId = this.props.match.params.grobljeId;
        if (this.props.currentGroblje === undefined || this.props.currentGroblje === null || this.props.currentGroblje !== grobljeId) {
            this.props.dispatch(grobljeActionCreator.fetchGroblje(grobljeId));
            this.props.dispatch(grobljeActionCreator.fetchPokojnikeNaGroblju(grobljeId));
            this.props.dispatch(grobljeActionCreator.fetchObavijesti(grobljeId));
        }
    }

    render() {

        if (this.props.currentGroblje === null) {
            return <ErrorPage errorNumber="404" errorMessage="Can't find groblje with given id"/>
        }

        return (
            <div>
                <Header/>
                <UvodniZaslon
                    naziv={this.props.currentGroblje.naziv}
                    adresa={this.props.currentGroblje.adresa}
                    slika={this.props.currentGroblje.slika}/>
                <div id="main">
                    <Obavijesti/>
                    <Karta groblje={this.props.currentGroblje}/>
                    <Informacije groblje={this.props.currentGroblje}/>
                </div>
                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {currentGroblje: state.groblja.current}
};

export default connect(mapStateToProps)(GrobljePage);