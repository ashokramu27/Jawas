import React, {Component} from "react";
import './GrobljePage.css';
import Header from '../components/Header';
import HomePageUvodniZaslon from '../components/HomePageUvodniZaslon';
import Footer from '../components/Footer';

class HomePage extends Component {
    render() {
        return (
            <div>
                <Header/>
                <HomePageUvodniZaslon/>
                <Footer naslov="Gradska groblja"/>
            </div>
        )
    }
}


export default HomePage;