import React, {Component} from "react";
import {connect} from "react-redux";
import GrobnoMjesto from "./../components/GrobnoMjesto";
import grobnoMjestoActionCreator from "../actionCreators/grobnoMjestoActionCreator";

class GrobnoMjestoList extends Component {
    constructor (props) {
        super(props);
        this.props.dispatch(grobnoMjestoActionCreator.fetchGrobnaMjesta());
    }

    render () {
        let grobnaMjesta = [];
        if (this.props.grobnaMjesta) {
            grobnaMjesta = this.props.grobnaMjesta.all.map((x) => <GrobnoMjesto key={x.id} grobnoMjesto = {x}/>);
        }

        return (
            <div>
                <h1>Grobno mjesto</h1>
                <ul>
                    {grobnaMjesta}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {grobnaMjesta: state.grobnaMjesta}
};

export default connect(mapStateToProps)(GrobnoMjestoList);