import React, {Component} from "react";
import {connect} from "react-redux";
import Korisnik from "./../components/Korisnik";
import korisnikActionCreator from "./../actionCreators/korisnikActionCreator";
import Header from "./../components/Header";
import Footer from "./../components/Footer";
import './KorisnikList.css';

class KorisnikList extends Component {
    constructor(props) {
        super(props);
        this.props.dispatch(korisnikActionCreator.fetchKorisnici());
    }

    render() {
        let korisnici = [];
        if (this.props.korisnici) {
            korisnici = this.props.korisnici.all.map((x) => <Korisnik key={x.id} korisnik={x}/>);
        }

        return (
            <div>
                <Header/>
                <div className="table-wrapper">
                    <div className="container">
                        <h1>Korisnici</h1>
                        <table className="table">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Ime</th>
                                <th>Prezime</th>
                                <th>oib</th>
                                <th>korisničko ime</th>
                                <th>uloga</th>
                            </tr>
                            </thead>
                            <tbody>
                            {korisnici}
                            </tbody>
                        </table>
                    </div>
                </div>
                <Footer/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {korisnici: state.korisnici}
};

export default connect(mapStateToProps)(KorisnikList);