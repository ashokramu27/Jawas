import React, {Component} from "react";
import {connect} from "react-redux";
import Header from "./../components/Header";
import Footer from '../components/Footer';
import UvodniZaslon from '../components/GrobljePageComponents/UvodniZaslon';
import KrajolikItem from '../components/KrajolikItem';
import muzej from "./../static/muzej.jpg"

class KrajolikList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let groblja = [];
        if (this.props.groblja) {
            groblja = this.props.groblja.all.map((x) => <KrajolikItem key={x.id} groblje={x}/>);
        }

        return (
            <div>
                <div>
                    <Header/>
                    <UvodniZaslon naziv="Muzeji" slika={muzej}/>
                    <ul className="groblje-list">
                        {groblja}
                    </ul>
                    <Footer/>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {groblja: state.groblja}
};

export default connect(mapStateToProps)(KrajolikList);