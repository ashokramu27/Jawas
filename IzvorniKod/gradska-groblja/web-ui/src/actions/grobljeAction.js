import * as types from "./actionTypes";

export const fetchGroblja = ({status, data}) => {
    return {type: types.FETCH_GROBLJA, status, data}
};

export const fetchGroblje = ({status, data}) => {
    return {type: types.FETCH_GROBLJE, status, data}
};

export const fetchPokojnikeNaGroblju = ({status, data}) => {
    return {type: types.FETCH_POKOJNIKE_NA_GROBLJU, status, data}
};

export const fetchObavijesti = ({status, data}) => {
    return {type: types.FETCH_OBAVIJESTI, status, data}
};