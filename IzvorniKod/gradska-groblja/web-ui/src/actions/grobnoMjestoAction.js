import * as types from "./actionTypes";

export const fetchGrobnaMjesta = ({status, data}) => {
    return {type: types.FETCH_GROBNA_MJESTA, status, data}
};

export const fetchGrobnoMjesto = ({status, data}) => {
    return {type: types.FETCH_GROBNO_MJESTO, status, data}
};