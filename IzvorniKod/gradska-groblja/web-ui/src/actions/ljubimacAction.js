import * as types from "./actionTypes";

export const fetchLjubimce = ({status, data}) => {
    return {type: types.FETCH_LJUBIMCE, status, data}
};

export const fetchLjubimac = ({status, data}) => {
    return {type: types.FETCH_LJUBIMAC, status, data}
};

export const updateLjubimac = (data) => {
    return {type: types.UPDATE_LJUBIMAC, data}
};