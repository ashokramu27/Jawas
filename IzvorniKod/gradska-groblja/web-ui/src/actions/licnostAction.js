import * as types from "./actionTypes";

export const fetchLicnosti = ({status, data}) => {
    return {type: types.FETCH_LICNOSTI, status, data}
};

export const fetchLicnost = ({status, data}) => {
    return {type: types.FETCH_LICNOST, status, data}
};