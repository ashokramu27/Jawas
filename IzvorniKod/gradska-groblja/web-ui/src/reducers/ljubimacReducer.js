import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const ljubimacReducer = (state = initialState.ljubimci, action) => {
    switch (action.type) {
        case types.FETCH_LJUBIMCE:
            if (action.status === "success") {
                return {
                    ...state,
                    all: action.data
                };
            } else {
                return state;
            }
        case types.FETCH_LJUBIMAC:
            if (action.status === "success") {
                return {
                    ...state,
                    current: action.data
                };
            } else {
                return {
                    ...state,
                    current: null
                };
            }
        case types.UPDATE_LJUBIMAC:
            let all = state.all;
            if (!!all.find(x => x.id === action.data.id)) {
                all = all.map(x => x.id === action.data.id ? action.data : x);
            } else {
                all.push(action.data);
            }
            return {
                ...state,
                all: all
            };

        default:
            return state;
    }
};

export default ljubimacReducer;