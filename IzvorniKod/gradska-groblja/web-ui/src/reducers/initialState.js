const initialState = {
    ljubimci: {
        all: [],
        current: {}
    },
    korisnici: {
        all: [],
        current: {}
    },
    usluge: {
        all: [],
        current: {}
    },
    groblja: {
        all: [],
        current: {},
        pokojnici: [],
        obavijesti: []
    },
    sadrzaji: {
        all: [],
        current: {}
    },
    sprovodi: {
        all: [],
        current: {}
    },
    pokojnici: {
        all: [],
        current: {}
    },
    korisniciUslugaGrobnoMjesto: {
        all: [],
        current: {}
    },
    grobnaMjesta: {
        all: [],
        current: {}
    },
    krajolici: {
        all: [],
        current: {}
    },
    licnosti: {
        all: [],
        current: {}
    },
    session: {
        token: null,
        korisnik: null,
    }
};

export default initialState;