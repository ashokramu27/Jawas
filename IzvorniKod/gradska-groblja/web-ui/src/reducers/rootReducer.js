import ljubimacReducer from "./ljubimacReducer"
import grobljeReducer from "./grobljeReducer"
import sadrzajReducer from "./sadrzajReducer"
import korisnikReducer from "./korisnikReducer";
import uslugaReducer from "./uslugaReducer";
import sprovodReducer from "./sprovodReducer";
import pokojnikReducer from "./pokojnikReducer";
import korisnikUslugaGrobnoMjestoReducer from "./korisnikUslugaGrobnoMjestoReducer";
import grobnoMjestoReducer from "./grobnoMjestoReducer";
import krajolikReducer from "./krajolikReducer";
import licnostReducer from "./licnostReducer";
import sessionReducer from "./sessionReducer";

const rootReducer = {
    ljubimci: ljubimacReducer,
    groblja: grobljeReducer,
    sadrzaji: sadrzajReducer,
    korisnici: korisnikReducer,
    usluge: uslugaReducer,
    groblja: grobljeReducer,
    sadrzaji: sadrzajReducer,
    korisnici: korisnikReducer,
    sprovodi: sprovodReducer,
    pokojnici: pokojnikReducer,
    korisniciUslugaGrobnoMjesto: korisnikUslugaGrobnoMjestoReducer,
    grobnaMjesta: grobnoMjestoReducer,
    krajolici: krajolikReducer,
    licnosti: licnostReducer,
    session: sessionReducer,
};

export default rootReducer;