import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const uslugaReducer = (state = initialState.usluge, action) => {
    switch (action.type) {
        case types.FETCH_USLUGE:
            if (action.status === "success") {
                return {
                    ...state,
                    all: action.data
                };
            } else {
                return state;
            }
        case types.FETCH_USLUGA:
            if (action.status === "success") {
                return {
                    ...state,
                    current: action.data
                };
            } else {
                return {
                    ...state,
                    current: null
                };
            }
        default:
            return state;
    }
};

export default uslugaReducer;