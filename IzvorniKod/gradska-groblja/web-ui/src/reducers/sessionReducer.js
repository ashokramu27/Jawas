import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const sessionReducer = (state = initialState.session, action) => {
    switch (action.type) {
        case types.POST_REGISTRACIJA:
            if (action.error) {
                return {
                    ...state,
                    token: null,
                    korisnik: null,
                };
            } else {
                return {
                    ...state,
                    token: action.token,
                    korisnik: action.korisnik,
                };
            }
        case types.POST_PRIJAVA:
            if (action.error) {
                return {
                    ...state,
                    token: null,
                    korisnik: null,
                };
            } else {
                return {
                    ...state,
                    token: action.token,
                    korisnik: action.korisnik,
                };
            }
        case types.NONE_ODJAVA:
            return {
                ...state,
                token: null,
                korisnik: null,
            };
        default:
            return state;
    }
};

export default sessionReducer;