import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const korisnikUslugaGrobnoMjestoReducer = (state = initialState.korisniciUslugaGrobnoMjesto, action) => {
    switch (action.type) {
        case types.FETCH_KORISNICI_USLUGA_GROBNO_MJESTO:
            if (action.status === "success") {
                return {
                    ...state,
                    all: action.data
                };
            } else {
                return state;
            }
        case types.FETCH_KORISNICI_USLUGA_GROBNO_MJESTO:
            if (action.status === "success") {
                return {
                    ...state,
                    current: action.data
                };
            } else {
                return {
                    ...state,
                    current: null
                };
            }
        default:
            return state;
    }
};

export default korisnikUslugaGrobnoMjestoReducer;