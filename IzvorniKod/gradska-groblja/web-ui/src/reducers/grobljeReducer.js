import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const grobljeReducer = (state = initialState.groblja, action) => {
    switch (action.type) {
        case types.FETCH_GROBLJA:
            if (action.status === "success") {
                return {
                    ...state,
                    all: action.data
                };
            } else {
                return state;
            }
        case types.FETCH_GROBLJE:
            if (action.status === "success") {
                return {
                    ...state,
                    current: action.data
                };
            } else {
                return {
                    ...state,
                    current: null
                };
            }
        case types.FETCH_POKOJNIKE_NA_GROBLJU:
            if (action.status === "success") {
                return {
                    ...state,
                    pokojnici: action.data
                };
            } else {
                return {
                    ...state,
                    pokojnici: null
                };
            }
        case types.FETCH_OBAVIJESTI:
            if (action.status === "success") {
                return {
                    ...state,
                    obavijesti: action.data
                };
            } else {
                return {
                    ...state,
                    obavijesti: null
                };
            }
        default:
            return state;
    }
};

export default grobljeReducer;