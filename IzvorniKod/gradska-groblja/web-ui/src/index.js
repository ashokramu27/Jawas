import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import 'font-awesome/css/font-awesome.min.css';
import './index.css';
import React from "react";
import ReactDOM from "react-dom";
import configureStore from "./store/configureStore";
import { PersistGate } from 'redux-persist/es/integration/react';
import Provider from "react-redux/es/components/Provider";
import {BrowserRouter} from "react-router-dom";
import Main from "./components/Main";



const { store, persistor } = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <PersistGate persistor={persistor}>
            <BrowserRouter>
                <Main/>
            </BrowserRouter>
        </PersistGate>
    </Provider>,
    document.getElementById('root')
);
