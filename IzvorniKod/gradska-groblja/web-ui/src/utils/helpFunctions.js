export function formatDate(date) {
    let tmp = new Date(date);
    let day = tmp.getDate() < 10 ? "0" + tmp.getDate() : tmp.getDate();
    let month = (tmp.getMonth() + 1) < 10 ? "0" + (tmp.getMonth() + 1) : tmp.getMonth() + 1;
    return day + "." + month + "." + tmp.getFullYear() + ".";
}

export function getCurrentDate() {
    let d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}