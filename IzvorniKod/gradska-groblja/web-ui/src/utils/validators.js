import React from "react";
import validator from "validator";

export const required = (value) => {
    if (!value.toString().trim().length) {
        return (
            <span className="help-block">
                Ovo polje je neophodno!
            </span>
        );
    }
};

export const email = (value) => {
    if (!validator.isEmail(value)) {
        return (
            <span className="help-block">
                {value} nije važeći email!
            </span>
        );
    }
};

export const oib = (value) => {
    if (!/^(\d{11})$/.test(value)) {
        return (
            <span className="help-block">
                {value} nije vazeci OIB!
            </span>
        );
    }
}