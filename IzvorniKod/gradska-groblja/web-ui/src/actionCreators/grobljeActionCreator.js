import request from "../utils/backend";
import paths from "../constants/paths";
import * as grobljeAction from "../actions/grobljeAction";

export default {
    fetchGroblja() {
        return dispatch => {
            request
                .get(paths.restApi.groblja)
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(grobljeAction.fetchGroblja({status: "failure", data: err}));
                    } else {
                        dispatch(grobljeAction.fetchGroblja({status: "success", data: res.body}));
                    }
                });
        }
    },

    fetchGroblje(id) {
        return dispatch => {
            request
                .get(paths.restApi.fetchGroblje(id))
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(grobljeAction.fetchGroblje({status: "failure", data: err}));
                    } else {
                        dispatch(grobljeAction.fetchGroblje({status: "success", data: res.body}));
                    }
                });
        }
    },

    fetchPokojnikeNaGroblju(id) {
        return dispatch => {
            request
                .get(paths.restApi.fetchPokojnikeNaGroblju(id))
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(grobljeAction.fetchPokojnikeNaGroblju({status: "failure", data: err}));
                    } else {
                        dispatch(grobljeAction.fetchPokojnikeNaGroblju({status: "success", data: res.body}));
                    }
                });
        }
    },

    fetchObavijesti(id) {
        return dispatch => {
            request
                .get(paths.restApi.obavijesti(id))
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(grobljeAction.fetchObavijesti({status: "failure", data: err}));
                    } else {
                        dispatch(grobljeAction.fetchObavijesti({status: "success", data: res.body}));
                    }
                });
        }
    }
}