import request from "../utils/backend";
import paths from "../constants/paths";
import * as ljubimacAction from "../actions/ljubimacAction";

export default {
    fetchLjubimce() {
        return dispatch => {
            request
                .get(paths.restApi.ljubimci)
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(ljubimacAction.fetchLjubimce({status: "failure", data: err}));
                    } else {
                        dispatch(ljubimacAction.fetchLjubimce({status: "success", data: res.body}));
                    }
                });
        }
    },

    fetchLjubimac(id) {
        return dispatch => {
            request
                .get(paths.restApi.fetchLjubimac(id))
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(ljubimacAction.fetchLjubimac({status: "failure", data: err}));
                    } else {
                        dispatch(ljubimacAction.fetchLjubimac({status: "success", data: res.body}));
                    }
                });
        }
    },

    postLjubimac(data) {
        return (dispatch, getState) => {
            const token = getState().session.token;

            request
                .post(paths.restApi.ljubimciDodaj)
                .set('Content-type', 'application/json')
                .set('Authorization', `Bearer ${token}`)
                .send(data)
                .end((err, res) => {
                    if (!(err || !res.ok)) {
                        dispatch(ljubimacAction.updateLjubimac(res.body));
                    }
                });
        }
    }
}