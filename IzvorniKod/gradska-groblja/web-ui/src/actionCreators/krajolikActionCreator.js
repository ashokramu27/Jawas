import request from "../utils/backend";
import paths from "../constants/paths";
import * as krajolikAction from "../actions/krajolikAction";

export default {
    fetchKrajolike() {
        return dispatch => {
            request
                .get(paths.restApi.krajolici)
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(krajolikAction.fetchKrajolike({status: "failure", data: err}));
                    } else {
                        dispatch(krajolikAction.fetchKrajolike({status: "success", data: res.body}));
                    }
                });
        }
    },

    fetchKrajolik(id) {
        return dispatch => {
            request
                .get(paths.restApi.fetchKrajolik(id))
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(krajolikAction.fetchKrajolik({status: "failure", data: err}));
                    } else {
                        dispatch(krajolikAction.fetchKrajolik({status: "success", data: res.body}));
                    }
                });
        }
    }
}