import request from "../utils/backend";
import paths from "../constants/paths";
import * as korisnikAction from "../actions/korisnikAction";

export default {
    fetchKorisnici() {
        return dispatch => {
            request
                .get(paths.restApi.korisnici)
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(korisnikAction.fetchKorisnici({status: "failure", data: err}));
                    } else {
                        dispatch(korisnikAction.fetchKorisnici({status: "success", data: res.body}));
                    }
                });
        }
    },

    fetchKorisnik(id) {
        return dispatch => {
            request
                .get(paths.restApi.fetchKorisnik(id))
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(korisnikAction.fetchKorisnik({status: "failure", data: err}));
                    } else {
                        dispatch(korisnikAction.fetchKorisnik({status: "success", data: res.body}));
                    }
                });
        }
    }
}