import request from "../utils/backend";
import paths from "../constants/paths";
import * as sadrzajAction from "../actions/sadrzajAction";

export default {
    fetchSadrzaje() {
        return dispatch => {
            request
                .get(paths.restApi.sadrzaji)
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(sadrzajAction.fetchSadrzaje({status: "failure", data: err}));
                    } else {
                        dispatch(sadrzajAction.fetchSadrzaje({status: "success", data: res.body}));
                    }
                });
        }
    },

    fetchSadrzaj(id) {
        return dispatch => {
            request
                .get(paths.restApi.fetchSadrzaj(id))
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(sadrzajAction.fetchSadrzaj({status: "failure", data: err}));
                    } else {
                        dispatch(sadrzajAction.fetchSadrzaj({status: "success", data: res.body}));
                    }
                });
        }
    }
}