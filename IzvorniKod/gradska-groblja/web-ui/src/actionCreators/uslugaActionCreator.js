import request from "../utils/backend";
import paths from "../constants/paths";
import * as uslugaAction from "../actions/uslugaAction";

export default {
    fetchUsluge() {
        return dispatch => {
            request
                .get(paths.restApi.usluge)
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(uslugaAction.fetchUsluge({status: "failure", data: err}));
                    } else {
                        dispatch(uslugaAction.fetchUsluge({status: "success", data: res.body}));
                    }
                });
        }
    },

    fetchUsluga(id) {
        return dispatch => {
            request
                .get(paths.restApi.fetchUsluga(id))
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(uslugaAction.fetchUsluga({status: "failure", data: err}));
                    } else {
                        dispatch(uslugaAction.fetchUsluga({status: "success", data: res.body}));
                    }
                });
        }
    }
}