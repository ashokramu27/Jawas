import request from "../utils/backend";
import paths from "../constants/paths";
import * as licnostAction from "../actions/licnostAction";

export default {
    fetchLicnosti() {
        return dispatch => {
            request
                .get(paths.restApi.licnosti)
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(licnostAction.fetchLicnosti({status: "failure", data: err}));
                    } else {
                        dispatch(licnostAction.fetchLicnosti({status: "success", data: res.body}));
                    }
                });
        }
    },

    fetchLicnost(id) {
        return dispatch => {
            request
                .get(paths.restApi.fetchLicnost(id))
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(licnostAction.fetchLicnost({status: "failure", data: err}));
                    } else {
                        dispatch(licnostAction.fetchLicnost({status: "success", data: res.body}));
                    }
                });
        }
    }
}