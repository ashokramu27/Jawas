import request from "../utils/backend";
import paths from "../constants/paths";
import * as pokojnikAction from "../actions/pokojnikAction";

export default {
    fetchPokojnike() {
        return dispatch => {
            request
                .get(paths.restApi.pokojnici)
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(pokojnikAction.fetchPokojnike({status: "failure", data: err}));
                    } else {
                        dispatch(pokojnikAction.fetchPokojnike({status: "success", data: res.body}));
                    }
                });
        }
    },

    fetchPokojnika(id) {
        return dispatch => {
            request
                .get(paths.restApi.fetchPokojnika(id))
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(pokojnikAction.fetchPokojnika({status: "failure", data: err}));
                    } else {
                        dispatch(pokojnikAction.fetchPokojnika({status: "success", data: res.body}));
                    }
                });
        }
    }
}